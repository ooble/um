use std::fs;

use um::*;

#[test]
fn truthful_expression() -> Result<(), Box<dyn std::error::Error>> {
    let program = "$x = 1";
    let result = evaluate(program, ())?;

    assert!(result);
    Ok(())
}

#[test]
fn false_expression() -> Result<(), Box<dyn std::error::Error>> {
    let program = "$x = 1, $x = 2";
    let result = evaluate(program, ())?;

    assert!(!result);
    Ok(())
}

#[test]
fn a_file_exists() -> Result<(), Box<dyn std::error::Error>> {
    let dir = tempfile::tempdir()?;
    let path = dir.path().join("test.txt");
    fs::write(&path, "Hello, world!")?;

    let program = format!("io/file[path = {}]", path.to_string_lossy());
    let result = evaluate(&program, ())?;

    assert!(result);
    Ok(())
}

#[test]
fn a_http_request_succeeds() -> Result<(), Box<dyn std::error::Error>> {
    let mut http_server = mockito::Server::new();
    let url = http_server.url();
    let _mock = http_server
        .mock("GET", "/hello")
        .with_body("Hello, world!")
        .create();

    let program = format!(
        r#"io/http[url = "{url}/hello", response = io/http/response[status = 200, body = "Hello, world!"]]"#
    );
    let result = evaluate(&program, ())?;
    assert!(result);

    let program =
        format!(r#"io/http[url = "{url}/goodbye", response = io/http/response[status = 200]]"#);
    let result = evaluate(&program, ())?;
    assert!(!result);

    Ok(())
}
