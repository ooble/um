use um_language::syntax::*;

pub trait Renderer {
    fn render(&mut self, goal: &Goal) -> std::io::Result<()>;
}

impl Renderer for () {
    fn render(&mut self, _goal: &Goal) -> std::io::Result<()> {
        Ok(())
    }
}

pub struct SimpleRenderer<W> {
    output: W,
}

impl<W> SimpleRenderer<W> {
    pub fn new(output: W) -> Self {
        Self { output }
    }
}

impl<W: std::io::Write> Renderer for SimpleRenderer<W> {
    fn render(&mut self, goal: &Goal) -> std::io::Result<()> {
        writeln!(self.output, "{goal}")
    }
}

pub struct TreeRenderer<W> {
    output: W,
}

impl<W> TreeRenderer<W> {
    pub fn new(output: W) -> Self {
        Self { output }
    }
}

impl<W: std::io::Write> TreeRenderer<W> {
    fn render_tree(&mut self, goal: &Goal, indentation: &mut Vec<Indent>) -> std::io::Result<()> {
        match goal {
            Goal::Equal(a, b) => {
                self.render_node('=', indentation)?;
                indentation.push(Indent::Middle);
                self.render_term(a, indentation)?;
                indentation.pop();
                indentation.push(Indent::Last);
                self.render_term(b, indentation)?;
                indentation.pop();
                Ok(())
            }
            Goal::Conjunction(a, b) => self.render_binary_tree("AND", a, b, indentation),
            Goal::Disjunction(a, b) => self.render_binary_tree("OR", a, b, indentation),
            Goal::Structure(Structure { name, arguments }) => {
                self.render_node(name, indentation)?;
                self.render_arguments(arguments, indentation)
            }
        }
    }

    fn render_arguments(
        &mut self,
        arguments: &[Argument],
        indentation: &mut Vec<Indent>,
    ) -> std::io::Result<()> {
        if let Some((most, last)) = split_last(arguments) {
            indentation.push(Indent::Middle);
            for argument in most {
                self.render_argument(argument, indentation)?;
            }
            indentation.pop();
            indentation.push(Indent::Last);
            self.render_argument(last, indentation)?;
            indentation.pop();
        }
        Ok(())
    }

    fn render_argument(
        &mut self,
        argument: &Argument,
        indentation: &mut Vec<Indent>,
    ) -> std::io::Result<()> {
        match &argument.value {
            Term::Variable(_) | Term::Scalar(_) | Term::Access(_) => {
                self.render_node(argument, indentation)
            }
            Term::Structure(structure) => {
                self.render_node(
                    format!("{} = {}", argument.name, structure.name),
                    indentation,
                )?;
                self.render_arguments(&structure.arguments, indentation)
            }
        }
    }

    fn render_term(&mut self, term: &Term, indentation: &mut Vec<Indent>) -> std::io::Result<()> {
        match &term {
            Term::Variable(_) | Term::Scalar(_) | Term::Access(_) => {
                self.render_node(term, indentation)
            }
            Term::Structure(structure) => {
                self.render_node(&structure.name, indentation)?;
                self.render_arguments(&structure.arguments, indentation)
            }
        }
    }

    fn render_binary_tree(
        &mut self,
        node: impl std::fmt::Display,
        left: &Goal,
        right: &Goal,
        indentation: &mut Vec<Indent>,
    ) -> std::io::Result<()> {
        self.render_node(node, indentation)?;
        indentation.push(Indent::Middle);
        self.render_tree(left, indentation)?;
        indentation.pop();
        indentation.push(Indent::Last);
        self.render_tree(right, indentation)?;
        indentation.pop();
        Ok(())
    }

    fn render_node(
        &mut self,
        node: impl std::fmt::Display,
        indentation: &[Indent],
    ) -> std::io::Result<()> {
        if let Some((most, last)) = split_last(indentation) {
            for indent in most {
                let branch_symbol = match indent {
                    Indent::Middle => '│',
                    Indent::Last => ' ',
                };
                write!(self.output, "{branch_symbol}   ")?;
            }
            let branch_symbol = match last {
                Indent::Middle => '├',
                Indent::Last => '└',
            };
            write!(self.output, "{branch_symbol}── ")?;
        }
        writeln!(self.output, "{node}")
    }
}

impl<W: std::io::Write> Renderer for TreeRenderer<W> {
    fn render(&mut self, goal: &Goal) -> std::io::Result<()> {
        self.render_tree(goal, &mut vec![])
    }
}

#[derive(Clone, Copy)]
enum Indent {
    Middle,
    Last,
}

fn split_last<T>(input: &[T]) -> Option<(&[T], &T)> {
    input.last().map(|last| (&input[0..input.len() - 1], last))
}
