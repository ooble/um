mod renderer;

use um_logic::library::Library;
use um_logic::progress::EventX;

pub use renderer::*;

pub fn run(query: &str, renderer: impl Renderer) -> Result<(), Box<dyn std::error::Error>> {
    let result = evaluate(query, renderer)?;
    if result {
        println!("success");
    } else {
        println!("failure");
    }
    Ok(())
}

pub fn evaluate(
    query: &str,
    mut renderer: impl Renderer,
) -> Result<bool, Box<dyn std::error::Error>> {
    let library = stdlib();

    let syntax_goal = um_language::parse(query)?;
    renderer.render(&syntax_goal)?;

    let (goal, solver) = um_language::transform(syntax_goal, library)?;
    for event in solver
        .evaluate(&goal)
        .filter(move |event| event.goal_id == goal.id)
    {
        match event.inner {
            EventX::Success(_) => {
                log::info!(goal_id:% = event.goal_id; "success");
                return Ok(true);
            }
            EventX::Failure => {
                log::info!(goal_id:% = event.goal_id; "failure");
                return Ok(false);
            }
            EventX::Pending => {
                log::debug!(goal_id:% = event.goal_id; "pending");
            }
        }
    }
    Err("incomplete progress".into())
}

pub fn stdlib() -> Library {
    Library::new()
        .extend(um_lib_core::library())
        .extend(um_lib_io::library())
}
