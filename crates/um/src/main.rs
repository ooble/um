use std::process::ExitCode;

use clap::Parser;

use um::*;

#[derive(Parser)]
#[command(version, about)]
struct Args {
    query: String,
}

fn main() -> ExitCode {
    env_logger::init();

    let args = Args::parse();
    let renderer = TreeRenderer::new(std::io::stdout());
    match run(&args.query, renderer) {
        Ok(()) => ExitCode::SUCCESS,
        Err(error) => {
            eprintln!("ERROR:\n{error}");
            ExitCode::FAILURE
        }
    }
}
