mod integer;
mod structure;
mod time;

use um_logic::library::{Library, Resource};

pub fn library() -> Library {
    Library::new()
        .with(Resource::Structure(structure::get::DESCRIPTOR.clone()))
        .with(Resource::Structure(integer::plus::DESCRIPTOR.clone()))
        .with(Resource::Structure(time::DESCRIPTOR.clone()))
}
