use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "core/get",
        arguments: vec![VALUE.clone(), FIELD.clone()],
        output: Some(ArgumentDescriptor::output()),
        implementation: goal,
    }
    .into();
    pub(super) static ref VALUE: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("value"),
        descriptor: Descriptor::Unknown
    }
    .into();
    pub(super) static ref FIELD: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("field"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Atom),
    }
    .into();
}

#[allow(clippy::needless_pass_by_value)]
fn goal(values: Arguments) -> Goal {
    goal::get(
        values.lookup(&VALUE),
        values.lookup(&FIELD),
        values.lookup(&ArgumentDescriptor::output()),
    )
}
