use std::time::Duration;

use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "core/time",
        arguments: vec![ELAPSED.clone()],
        output: None,
        implementation: goal,
    }
    .into();
    static ref ELAPSED: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("elapsed"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
}

#[allow(clippy::needless_pass_by_value)]
fn goal(values: Arguments) -> Goal {
    let elapsed = values.lookup(&ELAPSED);
    resolved(elapsed, move |elapsed| match elapsed.as_ref() {
        TermX::Scalar(Scalar::Integer(elapsed_time_in_seconds)) => {
            let Ok(elapsed_time_in_seconds_non_negative) = u64::try_from(*elapsed_time_in_seconds)
            else {
                return goal::empty();
            };
            let elapsed_time = Duration::from_secs(elapsed_time_in_seconds_non_negative);
            std::thread::sleep(elapsed_time);
            goal::one()
        }
        _ => todo!("Time::goal"),
    })
}
