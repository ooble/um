use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "core/plus",
        arguments: vec![LEFT.clone(), RIGHT.clone()],
        output: Some(OUTPUT.clone()),
        implementation: goal,
    }
    .into();
    pub(super) static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("left"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
    pub(super) static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("right"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
    pub(super) static ref OUTPUT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("output"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
}

fn goal(values: Arguments) -> Goal {
    resolved(
        [values.lookup(&LEFT), values.lookup(&RIGHT)],
        move |[left, right]| match (left.as_ref(), right.as_ref()) {
            (TermX::Scalar(Scalar::Integer(left)), TermX::Scalar(Scalar::Integer(right))) => {
                goal::eq(values.lookup(&OUTPUT), Term::int(left + right))
            }
            _ => goal::empty(),
        },
    )
}

#[cfg(test)]
mod tests {
    use um_logic::Solver;

    use super::*;

    #[test]
    fn adds_numbers() {
        let mut solver = Solver::default();
        let v = solver.var();
        let goal = DESCRIPTOR
            .builder()
            .set(&LEFT, Scalar::Integer(3.into()))
            .set(&RIGHT, Scalar::Integer(2.into()))
            .var(&OUTPUT, v)
            .goal();

        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);
        assert_eq!(results[0].lookup(v), Term::int(5));
    }

    #[test]
    fn rejects_failed_addition() {
        let solver = Solver::default();
        let goal = DESCRIPTOR
            .builder()
            .set(&LEFT, Scalar::Integer(3.into()))
            .set(&RIGHT, Scalar::Integer(2.into()))
            .set(&OUTPUT, Scalar::Integer(9.into()))
            .goal();

        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 0);
    }
}
