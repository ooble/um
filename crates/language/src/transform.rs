use std::collections::{hash_map, HashMap};

use um_logic::goal;
use um_logic::library::{Library, Resource};
use um_logic::Solver;

use crate::syntax;
use crate::Result;

/// Transform the goal from a syntax tree into something evaluable.
pub fn transform(
    goal: syntax::Goal,
    library: Library,
) -> Result<(um_logic::Goal, um_logic::Solver)> {
    let transformer = Transformer::new(library);
    let mut state = State::default();
    let goal = transformer.transform_goal(&mut state, goal)?;
    let solver = into_solver(transformer, state);
    Ok((goal, solver))
}

#[allow(clippy::needless_pass_by_value)]
fn into_solver(transformer: Transformer, state: State) -> Solver {
    um_logic::Solver::new(transformer.library, state.fresh)
}

/// The state of a transformation.
///
/// This should not be reused across transformations, as variable bindings are remembered and will
/// be shared.
#[derive(Default)]
struct State {
    bound: HashMap<syntax::Variable, um_logic::Var>,
    fresh: um_logic::FreshVariables,
}

impl State {
    fn var(&mut self, name: syntax::Variable) -> um_logic::Var {
        match self.bound.entry(name) {
            hash_map::Entry::Occupied(entry) => *entry.get(),
            hash_map::Entry::Vacant(entry) => {
                let var = self.fresh.next();
                entry.insert(var);
                var
            }
        }
    }
}

/// The transformer.
struct Transformer {
    library: Library,
}

impl Transformer {
    fn new(library: Library) -> Self {
        Transformer { library }
    }

    fn transform_goal(&self, state: &mut State, goal: syntax::Goal) -> Result<um_logic::Goal> {
        match goal {
            syntax::Goal::Equal(a, b) => {
                let (goal_a, term_a) = self.transform_term(state, a)?;
                let (goal_b, term_b) = self.transform_term(state, b)?;
                Ok(combine_goals(
                    goal::eq(term_a, term_b),
                    goal_a.into_iter().chain(goal_b),
                ))
            }
            syntax::Goal::Conjunction(a, b) => Ok(goal::conj(
                self.transform_goal(state, *a)?,
                self.transform_goal(state, *b)?,
            )),
            syntax::Goal::Disjunction(a, b) => Ok(goal::disj(
                self.transform_goal(state, *a)?,
                self.transform_goal(state, *b)?,
            )),
            syntax::Goal::Structure(structure) => self.transform_structure(state, structure, None),
        }
    }

    fn transform_structure(
        &self,
        state: &mut State,
        structure: syntax::Structure,
        output: Option<um_logic::Var>,
    ) -> Result<um_logic::Goal> {
        let Some(resource) = self.library.lookup(&structure.name) else {
            return Err(format!("Unknown name: {:?}", structure.name).into());
        };

        // This is split from the above match to ensure we handle extra cases in the future.
        let Resource::Structure(descriptor) = resource;

        let mut arguments: Vec<syntax::Argument> = structure.arguments;

        let mut goals: Vec<um_logic::Goal> = vec![];
        let mut values: um_logic::structure::Arguments =
            um_logic::structure::Arguments::new(&descriptor);
        for argument_descriptor in descriptor.arguments() {
            let mut found: Option<syntax::Term> = None;
            for i in 0..arguments.len() {
                if arguments[i].name == argument_descriptor.name.as_str() {
                    found = Some(arguments.swap_remove(i).value);
                    break;
                }
            }
            let (auxiliary_goal, term) = match found {
                Some(argument) => self.transform_term(state, argument)?,
                None => (None, um_logic::Term::ignored()),
            };
            if let Some(auxiliary_goal) = auxiliary_goal {
                goals.push(auxiliary_goal);
            }
            values = values.with(&argument_descriptor, term);
        }

        if let Some(output_descriptor) = descriptor.output() {
            if let Some(output) = output {
                values = values.with(&output_descriptor, um_logic::Term::var(output));
            }
        }

        if let Some(remaining) = arguments.first() {
            return Err(format!(
                "Unknown key for structure {:?}: {:?}",
                structure.name, remaining.name
            )
            .into());
        }

        let goal = descriptor.goal(values);

        Ok(combine_goals(goal, goals))
    }

    // TODO: Implement some kind of basic type checking.
    //
    // This currently does not accept a descriptor, and therefore is totally unaware of the
    // intended type for scalars or variables.
    fn transform_term(
        &self,
        state: &mut State,
        term: syntax::Term,
    ) -> Result<(Option<um_logic::Goal>, um_logic::Term)> {
        match term {
            syntax::Term::Variable(name) => Ok((None, um_logic::Term::var(state.var(name)))),
            syntax::Term::Scalar(scalar) => Ok((None, um_logic::Term::scalar(scalar))),
            syntax::Term::Structure(structure) => {
                let var = state.fresh.next();
                let goal = self.transform_structure(state, structure, Some(var))?;
                Ok((Some(goal), um_logic::Term::var(var)))
            }
            syntax::Term::Access(access) => {
                let var = state.fresh.next();
                let (value_goal, value) = self.transform_term(state, *access.term)?;
                let field = um_logic::Term::atom(&access.name);
                let output = um_logic::Term::var(var);
                let output_goal = um_logic::get(value, field, output.clone());
                let goal = if let Some(value_goal) = value_goal {
                    value_goal & output_goal
                } else {
                    output_goal
                };
                Ok((Some(goal), output))
            }
        }
    }
}

fn combine_goals(
    initial_goal: um_logic::Goal,
    auxiliary_goals: impl IntoIterator<Item = um_logic::Goal, IntoIter: DoubleEndedIterator>,
) -> um_logic::Goal {
    auxiliary_goals
        .into_iter()
        .rev()
        .fold(initial_goal, |g, x| x & g)
}

#[cfg(test)]
mod tests {
    use um_logic::descriptor::*;
    use um_logic::structure;

    use crate::syntax::*;

    use super::*;

    #[test]
    fn transforms_successful_equality() -> anyhow::Result<()> {
        let input = Goal::Equal(
            Term::Scalar(Scalar::Integer(123)),
            Term::Variable("value".into()),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        Ok(())
    }

    #[test]
    fn transforms_failing_equality() -> anyhow::Result<()> {
        let input = Goal::Equal(
            Term::Scalar(Scalar::Integer(123)),
            Term::Scalar(Scalar::Integer(456)),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 0);
        Ok(())
    }

    #[test]
    fn transforms_successful_conjunctions() -> anyhow::Result<()> {
        let input = Goal::Conjunction(
            Goal::Equal(
                Term::Scalar(Scalar::Integer(1)),
                Term::Variable("value".into()),
            )
            .into(),
            Goal::Equal(
                Term::Variable("value".into()),
                Term::Variable("output".into()),
            )
            .into(),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        Ok(())
    }

    #[test]
    fn transforms_failing_conjunctions() -> anyhow::Result<()> {
        let input = Goal::Conjunction(
            Goal::Equal(
                Term::Scalar(Scalar::Integer(1)),
                Term::Variable("value".into()),
            )
            .into(),
            Goal::Equal(
                Term::Variable("value".into()),
                Term::Scalar(Scalar::Integer(2)),
            )
            .into(),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 0);
        Ok(())
    }

    #[test]
    fn transforms_successful_disjunctions() -> anyhow::Result<()> {
        let input = Goal::Disjunction(
            Goal::Equal(
                Term::Variable("value".into()),
                Term::Scalar(Scalar::Integer(123)),
            )
            .into(),
            Goal::Equal(
                Term::Variable("value".into()),
                Term::Scalar(Scalar::Integer(456)),
            )
            .into(),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 2);
        Ok(())
    }

    #[test]
    fn transforms_failing_disjunctions() -> anyhow::Result<()> {
        let input = Goal::Disjunction(
            Goal::Equal(
                Term::Scalar(Scalar::Integer(1)),
                Term::Scalar(Scalar::Integer(2)),
            )
            .into(),
            Goal::Equal(
                Term::Scalar(Scalar::Integer(3)),
                Term::Scalar(Scalar::Integer(4)),
            )
            .into(),
        );

        let (goal, solver) = transform(input, Library::new())?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 0);
        Ok(())
    }

    #[test]
    fn transforms_structures() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(eq::DESCRIPTOR.clone()));
        let input = Goal::Structure(Structure {
            name: "test/eq".to_owned(),
            arguments: vec![
                Argument {
                    name: "left".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
                Argument {
                    name: "right".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
            ],
        });

        let (goal, solver) = transform(input, library)?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        Ok(())
    }

    #[test]
    fn transforms_structures_with_missing_arguments() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(eq::DESCRIPTOR.clone()));
        let input = Goal::Structure(Structure {
            name: "test/eq".to_owned(),
            arguments: vec![Argument {
                name: "left".to_owned(),
                value: Term::Scalar(Scalar::String("value".to_owned())),
            }],
        });

        let (goal, solver) = transform(input, library)?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        Ok(())
    }

    #[test]
    fn transforms_failing_structures() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(eq::DESCRIPTOR.clone()));
        let input = Goal::Structure(Structure {
            name: "test/eq".to_owned(),
            arguments: vec![
                Argument {
                    name: "left".to_owned(),
                    value: Term::Scalar(Scalar::String("one".to_owned())),
                },
                Argument {
                    name: "right".to_owned(),
                    value: Term::Scalar(Scalar::String("two".to_owned())),
                },
            ],
        });

        let (goal, solver) = transform(input, library)?;
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 0);
        Ok(())
    }

    #[test]
    fn rejects_structures_with_incorrect_argument_names() {
        let library = Library::new().with(Resource::Structure(eq::DESCRIPTOR.clone()));
        let input = Goal::Structure(Structure {
            name: "test/eq".to_owned(),
            arguments: vec![
                Argument {
                    name: "a".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
                Argument {
                    name: "b".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
            ],
        });

        if let Err(message) = transform(input, library) {
            assert_eq!(
                &message.to_string(),
                r#"Unknown key for structure "test/eq": "a""#
            );
        } else {
            panic!("Unexpected success.");
        }
    }

    #[test]
    fn rejects_structures_with_duplicate_argument_names() {
        let library = Library::new().with(Resource::Structure(eq::DESCRIPTOR.clone()));
        let input = Goal::Structure(Structure {
            name: "test/eq".to_owned(),
            arguments: vec![
                Argument {
                    name: "left".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
                Argument {
                    name: "left".to_owned(),
                    value: Term::Scalar(Scalar::String("value".to_owned())),
                },
            ],
        });

        if let Err(message) = transform(input, library) {
            assert_eq!(
                &message.to_string(),
                r#"Unknown key for structure "test/eq": "left""#
            );
        } else {
            panic!("Unexpected success.");
        }
    }

    #[test]
    fn transforms_structures_with_outputs_via_equality() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(plus::DESCRIPTOR.clone()));
        let input = Goal::Equal(
            Term::Variable("output".into()),
            Term::Structure(Structure {
                name: "test/plus".to_owned(),
                arguments: vec![
                    Argument {
                        name: "left".to_owned(),
                        value: Term::Scalar(Scalar::Integer(3)),
                    },
                    Argument {
                        name: "right".to_owned(),
                        value: Term::Scalar(Scalar::Integer(2)),
                    },
                ],
            }),
        );

        let transformer = Transformer::new(library);
        let mut state = State::default();
        let output = state.var("output".into());
        let goal = transformer.transform_goal(&mut state, input)?;

        let solver = into_solver(transformer, state);
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        assert_eq!(
            solutions[0].lookup(output),
            um_logic::Term::scalar(Scalar::Integer(5)),
        );
        Ok(())
    }

    #[test]
    fn transforms_structure_access_with_get() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(duo::DESCRIPTOR.clone()));
        let input = Goal::Conjunction(
            Goal::Equal(
                Term::Variable("duo".into()),
                Term::Structure(Structure {
                    name: "test/duo".to_owned(),
                    arguments: vec![
                        Argument {
                            name: "left".to_owned(),
                            value: Term::Scalar(Scalar::String("Garfunkel".to_owned())),
                        },
                        Argument {
                            name: "right".to_owned(),
                            value: Term::Scalar(Scalar::String("Oates".to_owned())),
                        },
                    ],
                }),
            )
            .into(),
            Goal::Equal(
                Term::Access(Access {
                    term: Term::Variable("duo".into()).into(),
                    name: "left".to_owned(),
                }),
                Term::Variable("output".into()),
            )
            .into(),
        );

        let transformer = Transformer::new(library);
        let mut state = State::default();
        let output = state.var("output".into());
        let goal = transformer.transform_goal(&mut state, input)?;

        let solver = into_solver(transformer, state);
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 1);
        assert_eq!(
            solutions[0].lookup(output),
            um_logic::Term::string("Garfunkel"),
        );
        Ok(())
    }

    #[test]
    fn does_not_reallocate_used_variables() -> anyhow::Result<()> {
        let library = Library::new().with(Resource::Structure(plus::DESCRIPTOR.clone()));
        let input = Goal::Conjunction(
            Goal::Equal(
                Term::Variable("output".into()),
                Term::Structure(Structure {
                    name: "test/plus".to_owned(),
                    arguments: vec![
                        Argument {
                            name: "left".to_owned(),
                            value: Term::Scalar(Scalar::Integer(3)),
                        },
                        Argument {
                            name: "right".to_owned(),
                            value: Term::Scalar(Scalar::Integer(2)),
                        },
                    ],
                }),
            )
            .into(),
            Goal::Equal(
                Term::Variable("output".into()),
                Term::Scalar(Scalar::Integer(7)),
            )
            .into(),
        );

        let transformer = Transformer::new(library);
        let mut state = State::default();
        let goal = transformer.transform_goal(&mut state, input)?;

        let solver = into_solver(transformer, state);
        let solutions = solver.solve(&goal).collect::<Vec<_>>();

        assert_eq!(solutions.len(), 0);
        Ok(())
    }

    mod eq {
        use um_logic::lazy_static;

        use super::*;

        lazy_static! {
            pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> =
                StructureDescriptor::Native {
                    name: "test/eq",
                    arguments: vec![LEFT.clone(), RIGHT.clone()],
                    output: None,
                    implementation: goal,
                }
                .into();
            static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("left"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
            }
            .into();
            static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("right"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
            }
            .into();
        }

        #[allow(clippy::needless_pass_by_value)]
        fn goal(values: structure::Arguments) -> goal::Goal {
            goal::eq(values.lookup(&LEFT), values.lookup(&RIGHT))
        }
    }

    mod plus {
        use um_logic::lazy_static;

        use super::*;

        lazy_static! {
            pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> =
                StructureDescriptor::Native {
                    name: "test/plus",
                    arguments: vec![LEFT.clone(), RIGHT.clone()],
                    output: Some(OUTPUT.clone()),
                    implementation: goal,
                }
                .into();
            static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("left"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
            }
            .into();
            static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("right"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
            }
            .into();
            static ref OUTPUT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("output"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
            }
            .into();
        }

        fn goal(values: structure::Arguments) -> goal::Goal {
            goal::resolved(
                [values.lookup(&LEFT), values.lookup(&RIGHT)],
                move |[left, right]| match (left.as_ref(), right.as_ref()) {
                    (
                        um_logic::TermX::Scalar(Scalar::Integer(left)),
                        um_logic::TermX::Scalar(Scalar::Integer(right)),
                    ) => goal::eq(values.lookup(&OUTPUT), um_logic::Term::int(left + right)),
                    _ => goal::empty(),
                },
            )
        }
    }

    mod duo {
        use um_logic::lazy_static;

        use super::*;

        lazy_static! {
            pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> =
                StructureDescriptor::Data {
                    name: "test/duo",
                    inputs: vec![LEFT.clone(), RIGHT.clone()],
                    output: ArgumentDescriptor::output(),
                }
                .into();
            static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("left"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::String),
            }
            .into();
            static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
                name: ArgumentName::of_static("right"),
                descriptor: Descriptor::Scalar(ScalarDescriptor::String),
            }
            .into();
        }
    }
}
