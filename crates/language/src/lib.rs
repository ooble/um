mod parse;
pub mod syntax;
mod transform;

use lalrpop_util::lalrpop_mod;

lalrpop_mod!(
    #[allow(clippy::pedantic)]
    grammar
);

// TODO: Come up with a better error type than `String`.
#[derive(Debug, PartialEq, Eq)]
pub struct Error(String);

impl From<String> for Error {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl<L: std::fmt::Display, T: std::fmt::Display, E: std::fmt::Display>
    From<lalrpop_util::ParseError<L, T, E>> for Error
{
    fn from(value: lalrpop_util::ParseError<L, T, E>) -> Self {
        Self(value.to_string())
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;

pub use parse::parse;
pub use transform::transform;
