pub use um_logic::Scalar;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Variable(String);

impl std::fmt::Display for Variable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "${}", self.0)
    }
}

impl From<String> for Variable {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl From<&str> for Variable {
    fn from(value: &str) -> Self {
        Self(value.to_owned())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Goal {
    Equal(Term, Term),
    Conjunction(Box<Goal>, Box<Goal>),
    Disjunction(Box<Goal>, Box<Goal>),
    Structure(Structure),
}

impl std::fmt::Display for Goal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Equal(a, b) => write!(f, "{a} = {b}"),
            Self::Conjunction(a, b) => write!(f, "({a}, {b})"),
            Self::Disjunction(a, b) => write!(f, "({a} | {b})"),
            Self::Structure(structure) => write!(f, "{structure}"),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Term {
    Variable(Variable),
    Scalar(Scalar),
    Structure(Structure),
    Access(Access),
}

impl std::fmt::Display for Term {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Term::Variable(variable) => write!(f, "{variable}"),
            Term::Scalar(scalar) => write!(f, "{scalar}"),
            Term::Structure(structure) => write!(f, "{structure}"),
            Term::Access(access) => write!(f, "{access}"),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Structure {
    pub name: String,
    pub arguments: Arguments,
}

impl std::fmt::Display for Structure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}[", self.name)?;
        let mut arguments = self.arguments.iter();
        if let Some(argument) = arguments.next() {
            write!(f, "{argument}")?;
            for argument in arguments {
                write!(f, ", {argument}")?;
            }
        }
        write!(f, "]")
    }
}

pub type Arguments = Vec<Argument>;

#[derive(Debug, PartialEq, Eq)]
pub struct Argument {
    pub name: String,
    pub value: Term,
}

impl std::fmt::Display for Argument {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} = {}", self.name, self.value)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Access {
    pub term: Box<Term>,
    pub name: String,
}

impl std::fmt::Display for Access {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}", self.term, self.name)
    }
}
