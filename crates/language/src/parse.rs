use crate::grammar;
use crate::syntax::*;
use crate::Result;

/// Parse the input string.
pub fn parse(input: &str) -> Result<Goal> {
    let parser = grammar::GoalParser::new();
    let goal = parser.parse(input)?;
    Ok(goal)
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use assert_matches::assert_matches;

    use super::*;

    impl From<Scalar> for Term {
        fn from(value: Scalar) -> Self {
            Self::Scalar(value)
        }
    }

    #[test]
    fn parses_atoms() {
        let parser = grammar::TermParser::new();

        assert_eq!(parser.parse("a"), Ok(Scalar::Atom("a".to_owned()).into()));
        assert_eq!(
            parser.parse("thing"),
            Ok(Scalar::Atom("thing".to_owned()).into())
        );
        assert_eq!(
            parser.parse("LOUD"),
            Ok(Scalar::Atom("LOUD".to_owned()).into())
        );
        assert_eq!(
            parser.parse("this_IS_a_22-char_atom"),
            Ok(Scalar::Atom("this_IS_a_22-char_atom".to_owned()).into())
        );
    }

    #[test]
    fn parses_numbers() {
        let parser = grammar::TermParser::new();

        assert_eq!(parser.parse("0"), Ok(Scalar::Integer(0).into()));
        assert_eq!(parser.parse("123"), Ok(Scalar::Integer(123).into()));
        assert_eq!(parser.parse("-5"), Ok(Scalar::Integer(-5).into()));
    }

    #[test]
    fn parses_strings() {
        let parser = grammar::TermParser::new();

        assert_eq!(
            parser.parse(r#""abc""#),
            Ok(Scalar::String("abc".to_owned()).into())
        );
        assert_eq!(
            parser.parse(r#""this is a long string full of nonsense""#),
            Ok(Scalar::String("this is a long string full of nonsense".to_owned()).into())
        );
        assert_eq!(
            parser.parse(r#""this string contains so-called \"quotes\"""#),
            Ok(Scalar::String("this string contains so-called \"quotes\"".to_owned()).into())
        );
    }

    #[test]
    fn parses_absolute_unix_paths() {
        let parser = grammar::TermParser::new();

        let valid_paths = vec![
            "/",
            "/path/to/file.extension",
            "/path/to/file_without_extension",
            "/directory/with/trailing/slash/",
        ];
        for path in valid_paths {
            assert_eq!(
                parser.parse(path),
                Ok(Scalar::Path(PathBuf::from(path)).into())
            );
        }
    }

    #[test]
    fn parses_relative_unix_paths() {
        let parser = grammar::TermParser::new();

        let valid_paths = vec![
            "./",
            "./.",
            "./file.extension",
            "./file_without_extension",
            "./..",
            "./../file",
        ];
        for path in valid_paths {
            assert_eq!(
                parser.parse(path),
                Ok(Scalar::Path(PathBuf::from(path)).into())
            );
        }
    }

    #[test]
    fn parses_home_directory_unix_paths() {
        let parser = grammar::TermParser::new();

        let valid_paths = vec![
            "~/",
            "~/.",
            "~/file.extension",
            "~/file_without_extension",
            "~/path/to/file",
            "~/path/to/directory_with_trailing_slash/",
        ];
        for path in valid_paths {
            assert_eq!(
                parser.parse(path),
                Ok(Scalar::Path(PathBuf::from(path)).into())
            );
        }
    }

    #[test]
    fn rejects_paths_with_special_characters() {
        let parser = grammar::TermParser::new();

        let invalid_characters = vec!['\\', ',', ';', '[', ']', '(', ')', '{', '}', '<', '>', '&'];
        for character in invalid_characters {
            assert_matches!(
                parser.parse(&format!("/path/with/{character}etc")),
                Err(lalrpop_util::ParseError::InvalidToken { .. }
                    | lalrpop_util::ParseError::UnrecognizedToken { .. })
            );
        }
    }

    #[test]
    fn parses_variables() {
        let parser = grammar::TermParser::new();

        assert_eq!(parser.parse("$v"), Ok(Term::Variable("v".into())));
        assert_eq!(
            parser.parse("$variable"),
            Ok(Term::Variable("variable".into()))
        );
        assert_matches!(
            parser.parse("$123"),
            Err(lalrpop_util::ParseError::UnrecognizedToken { .. })
        );
        // TODO:
        // assert_matches!(
        //     parser.parse("$ separate"),
        //     Err(lalrpop_util::ParseError::Something { .. })
        // );
    }

    #[test]
    fn parses_equality() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$value = 1"),
            Ok(Goal::Equal(
                Term::Variable("value".into()),
                Scalar::Integer(1).into()
            ))
        );
    }

    #[test]
    fn parses_conjunctions() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$a = $b, $b = 3"),
            Ok(Goal::Conjunction(
                Goal::Equal(Term::Variable("a".into()), Term::Variable("b".into())).into(),
                Goal::Equal(Term::Variable("b".into()), Scalar::Integer(3).into()).into(),
            ))
        );
    }

    #[test]
    fn parses_disjunctions() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$thing = 123 | $thing = 456"),
            Ok(Goal::Disjunction(
                Goal::Equal(Term::Variable("thing".into()), Scalar::Integer(123).into()).into(),
                Goal::Equal(Term::Variable("thing".into()), Scalar::Integer(456).into()).into(),
            ))
        );
    }

    #[test]
    fn conjunctions_take_precedence_over_disjunctions() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$input = 1, $output = pass | $input = 2, $output = fail"),
            Ok(Goal::Disjunction(
                Goal::Conjunction(
                    Goal::Equal(Term::Variable("input".into()), Scalar::Integer(1).into()).into(),
                    Goal::Equal(
                        Term::Variable("output".into()),
                        Scalar::Atom("pass".into()).into()
                    )
                    .into(),
                )
                .into(),
                Goal::Conjunction(
                    Goal::Equal(Term::Variable("input".into()), Scalar::Integer(2).into()).into(),
                    Goal::Equal(
                        Term::Variable("output".into()),
                        Scalar::Atom("fail".to_owned()).into()
                    )
                    .into(),
                )
                .into(),
            ))
        );
    }

    #[test]
    fn precedence_can_be_overridden_with_parentheses() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("($input = 1 | $input = 2), $output = $input"),
            Ok(Goal::Conjunction(
                Goal::Disjunction(
                    Goal::Equal(Term::Variable("input".into()), Scalar::Integer(1).into()).into(),
                    Goal::Equal(Term::Variable("input".into()), Scalar::Integer(2).into()).into(),
                )
                .into(),
                Goal::Equal(
                    Term::Variable("output".into()),
                    Term::Variable("input".into()),
                )
                .into(),
            ))
        );
    }

    #[test]
    fn associates_to_the_right() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$value = 1 | $value = 2 | $value = 3"),
            Ok(Goal::Disjunction(
                Goal::Equal(Term::Variable("value".into()), Scalar::Integer(1).into()).into(),
                Goal::Disjunction(
                    Goal::Equal(Term::Variable("value".into()), Scalar::Integer(2).into()).into(),
                    Goal::Equal(Term::Variable("value".into()), Scalar::Integer(3).into()).into(),
                )
                .into()
            ))
        );
    }

    #[test]
    fn parses_compound_structures() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("plus[left = 1, right = 2, output = $output]"),
            Ok(Goal::Structure(Structure {
                name: "plus".to_owned(),
                arguments: vec![
                    Argument {
                        name: "left".to_owned(),
                        value: Scalar::Integer(1).into()
                    },
                    Argument {
                        name: "right".to_owned(),
                        value: Scalar::Integer(2).into()
                    },
                    Argument {
                        name: "output".to_owned(),
                        value: Term::Variable("output".into()),
                    }
                ]
            }))
        );
    }

    #[test]
    fn parses_compound_structures_with_assignment() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse("$output = plus[left = 1, right = 2]"),
            Ok(Goal::Equal(
                Term::Variable("output".into()),
                Term::Structure(Structure {
                    name: "plus".to_owned(),
                    arguments: vec![
                        Argument {
                            name: "left".to_owned(),
                            value: Scalar::Integer(1).into()
                        },
                        Argument {
                            name: "right".to_owned(),
                            value: Scalar::Integer(2).into()
                        },
                    ]
                }),
            ))
        );
    }

    #[test]
    fn parses_structure_access() {
        let parser = grammar::TermParser::new();

        assert_eq!(
            parser.parse("$thing.value"),
            Ok(Term::Access(Access {
                term: Term::Variable("thing".into()).into(),
                name: "value".to_owned(),
            }))
        );
    }

    #[test]
    fn strings_do_not_go_past_the_closing_quote() {
        let parser = grammar::GoalParser::new();

        assert_eq!(
            parser.parse(r#"$input = "one" | $input = "two""#),
            Ok(Goal::Disjunction(
                Goal::Equal(
                    Term::Variable("input".into()),
                    Scalar::String("one".into()).into()
                )
                .into(),
                Goal::Equal(
                    Term::Variable("input".into()),
                    Scalar::String("two".to_owned()).into()
                )
                .into(),
            ))
        );
    }

    #[test]
    fn ignores_leading_and_trailing_whitespace() {
        let parser = grammar::TermParser::new();

        assert_eq!(parser.parse("   7   "), Ok(Scalar::Integer(7).into()));
    }

    #[test]
    fn does_not_parse_anything_else() {
        let parser = grammar::TermParser::new();

        assert_eq!(
            parser.parse("  !"),
            Err(lalrpop_util::ParseError::InvalidToken { location: 2 })
        );
    }
}
