use std::sync::Arc;

use crate::context::Context;
use crate::descriptor::{ArgumentDescriptor, ArgumentName, Ref, StructureDescriptor};
use crate::goal::{Goal, GoalImpl, GoalOutcome};
use crate::scalar::Scalar;
use crate::term::Term;
use crate::variable::Var;

pub type Binding = Arc<dyn Fn(Var) -> Goal + Send + Sync>;
pub type Bindings = im::Vector<(Ref<ArgumentDescriptor>, Binding)>;

struct StructureGoal {
    descriptor: Ref<StructureDescriptor>,
    values: Arguments,
    bindings: Bindings,
}

impl GoalImpl for StructureGoal {
    fn run_with(&self, mut context: Context) -> GoalOutcome {
        let mut auxiliary_goals = vec![];
        let mut values = self.values.clone();
        // For each nested binding,
        for (bound_argument, binding) in &self.bindings {
            // allocate a new variable,
            let var = context.goal.next_var();
            let var_term = Term::var(var);
            // construct the binding's goal with that binding,
            auxiliary_goals.push(binding(var));
            // and assign it to the appropriate argument.
            values = values.with(bound_argument, var_term);
        }
        // Construct the final goal with all bound values.
        let final_goal = self.descriptor.goal(values);
        // Apply the auxiliary goals first.
        let goal = auxiliary_goals
            .into_iter()
            .rev()
            .fold(final_goal, |acc, g| g & acc);
        goal.run_with(context).into()
    }
}

/// A builder for a compound structure.
#[derive(Clone)]
pub struct StructureBuilder {
    descriptor: Ref<StructureDescriptor>,
    values: Arguments,
    bindings: Bindings,
}

impl StructureBuilder {
    /// Constructs a new builder whose values are unknown.
    pub fn new(descriptor: Ref<StructureDescriptor>) -> Self {
        let values = Arguments::new(&descriptor);
        Self {
            descriptor,
            values,
            bindings: im::Vector::new(),
        }
    }

    /// Connects a value in the builder to a variable.
    pub fn var(&self, argument: &ArgumentDescriptor, var: Var) -> Self {
        self.with(argument, Term::var(var))
    }

    /// Fixes a value in the builder to a scalar value.
    pub fn set(&self, argument: &ArgumentDescriptor, scalar: Scalar) -> Self {
        self.with(argument, Term::scalar(scalar))
    }

    /// Fixes a value in the query to a nested structure.
    ///
    /// # Panics
    ///
    /// Panics if the structure does not have an output, and therefore cannot be nested.
    ///
    /// Panics if the argument is not specified in the descriptor.
    #[allow(clippy::needless_pass_by_value)]
    pub fn nest(&self, argument: Ref<ArgumentDescriptor>, structure: StructureBuilder) -> Self {
        self.check_argument(&argument);
        let binding: Binding = if let Some(output) = structure.descriptor.output() {
            Arc::new(move |var| structure.var(&output, var).goal())
        } else {
            panic!(
                "The structure cannot be nested: {:?}",
                structure.descriptor.name()
            )
        };
        let mut bindings = self.bindings.clone();
        bindings.push_back((argument, binding));
        Self {
            descriptor: self.descriptor.clone(),
            values: self.values.clone(),
            bindings,
        }
    }

    /// Sets a value in the builder to a term.
    ///
    /// # Panics
    ///
    /// Panics if the argument is not specified in the descriptor.
    pub fn with(&self, argument: &ArgumentDescriptor, value: Term) -> Self {
        self.check_argument(argument);
        Self {
            descriptor: self.descriptor.clone(),
            values: self.values.with(argument, value),
            bindings: self.bindings.clone(),
        }
    }

    /// Converts this into a finalized goal.
    pub fn goal(self) -> Goal {
        Goal::new(StructureGoal {
            descriptor: self.descriptor,
            values: self.values,
            bindings: self.bindings,
        })
    }

    fn check_argument(&self, argument: &ArgumentDescriptor) {
        self.descriptor
            .arguments()
            .find(|a| a.name == argument.name)
            .unwrap_or_else(|| {
                panic!(
                    "Invalid argument for {:?}: {:?}",
                    self.descriptor.name(),
                    argument.name
                )
            });
    }
}

/// A set of values in a structure, corresponding to the structure keys.
///
/// Defaults to all values "ignored".
#[derive(Debug, Clone)]
pub struct Arguments(im::OrdMap<ArgumentName, Term>);

impl FromIterator<(ArgumentName, Term)> for Arguments {
    fn from_iter<T: IntoIterator<Item = (ArgumentName, Term)>>(iter: T) -> Self {
        Self(iter.into_iter().collect())
    }
}

impl IntoIterator for Arguments {
    type Item = (ArgumentName, Term);

    type IntoIter = <im::OrdMap<ArgumentName, Term> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl Arguments {
    /// Constructs a set of name-value pairs, with all the terms set to "ignored".
    pub fn new(descriptor: &StructureDescriptor) -> Self {
        let arguments: Box<dyn Iterator<Item = Ref<ArgumentDescriptor>>> = match descriptor {
            StructureDescriptor::Data {
                name: _,
                inputs,
                output,
            } => Box::new(inputs.iter().chain([output]).cloned()),
            StructureDescriptor::Native {
                name: _,
                arguments,
                output,
                implementation: _,
            } => Box::new(arguments.iter().chain(output.iter()).cloned()),
        };
        arguments
            .map(|argument| (argument.name, Term::ignored()))
            .collect()
    }

    /// Binds an argument to a term.
    pub fn with(&self, argument: &ArgumentDescriptor, value: Term) -> Self {
        Self(self.0.update(argument.name, value))
    }

    /// Looks up the given name.
    ///
    /// # Panics
    ///
    /// Panics if the argument is not present in this set of arguments.
    pub fn lookup(&self, argument: &ArgumentDescriptor) -> Term {
        match self.0.get(&argument.name) {
            Some(value) => value.clone(),
            None => panic!("Unknown argument: {:?}", argument.name),
        }
    }

    /// Looks up the given name, and removes it.
    ///
    /// # Panics
    ///
    /// Panics if the argument is not present in this set of arguments.
    pub fn without(&self, argument: &ArgumentDescriptor) -> (Term, Self) {
        match self.0.extract(&argument.name) {
            Some((value, rest)) => (value, Self(rest)),
            None => panic!("Unknown argument: {:?}", argument.name),
        }
    }
}
