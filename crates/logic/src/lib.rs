pub mod context;
pub mod descriptor;
pub mod goal;
pub mod library;
pub mod outcome;
pub mod progress;
pub mod scalar;
pub mod solve;
pub mod structure;
pub mod substitution;
pub mod term;
pub mod variable;

mod thunk;

pub use goal::{conj, defer, disj, empty, eq, fresh, fresh_n, get, one, resolved, Goal};
pub use scalar::Scalar;
pub use solve::{Solution, Solver};
pub use substitution::Substitution;
pub use term::{Term, TermX};
pub use variable::{FreshVariables, Var};

pub use lazy_static::lazy_static;
