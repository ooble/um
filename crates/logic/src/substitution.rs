use crate::term::Term;
use crate::variable::Var;

/// Substitutions from variables to terms.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Substitution(im::OrdMap<Var, Term>);

impl Substitution {
    /// The empty substitution.
    pub fn empty() -> Self {
        Self(im::ordmap![])
    }

    /// A single substitution.
    pub fn of(lhs: Var, rhs: Term) -> Self {
        Substitution(im::ordmap![lhs => rhs])
    }

    /// Looks up the given term.
    pub(crate) fn lookup(&self, var: Var) -> Option<&Term> {
        self.0.get(&var)
    }

    /// Merges two substitutions.
    ///
    /// In the event of a collision, the terms are checked. If they are not identical, the merge
    /// fails. This check does not follow variables.
    pub(crate) fn merge(&self, other: &Self) -> Option<Self> {
        let mut merged = self.0.clone();
        for (var, term) in &other.0 {
            if merged.get(var).is_some_and(|original| original != term) {
                return None;
            }
            merged.insert(*var, term.clone());
        }
        Some(Self(merged))
    }
}
