use crate::structure;
use crate::Term;

pub type Ref<T> = std::sync::Arc<T>;

pub enum Descriptor {
    Unknown,
    Scalar(ScalarDescriptor),
    Structure(Ref<StructureDescriptor>),
}

/// A descriptor of a scalar.
pub enum ScalarDescriptor {
    Atom,
    Integer,
    String,
    Path,
}

/// A descriptor of a composite structure.
pub enum StructureDescriptor {
    /// A composite structure in which its inputs are isomorphic to its output.
    ///
    /// The output is implementation-defined, but is typically the product of
    /// all the inputs.
    Data {
        /// The canonical name of the composite structure.
        name: &'static str,
        /// The inputs.
        inputs: Vec<Ref<ArgumentDescriptor>>,
        /// The output.
        output: Ref<ArgumentDescriptor>,
    },
    /// A composite structure implemented with native code.
    Native {
        /// The canonical name of the composite structure.
        name: &'static str,
        /// The arguments of the structure.
        arguments: Vec<Ref<ArgumentDescriptor>>,
        /// An optional output descriptor.
        output: Option<Ref<ArgumentDescriptor>>,
        /// A reference to the implementation so it can be run.
        implementation: fn(values: structure::Arguments) -> crate::Goal,
    },
}

impl StructureDescriptor {
    /// The canonical name of the composite structure.
    pub fn name(&self) -> &'static str {
        match self {
            StructureDescriptor::Data { name, .. } | StructureDescriptor::Native { name, .. } => {
                name
            }
        }
    }

    pub fn arguments(&self) -> Box<dyn Iterator<Item = Ref<ArgumentDescriptor>> + '_> {
        match self {
            Self::Data {
                name: _,
                inputs,
                output,
            } => Box::new(inputs.iter().chain([output]).cloned()),
            Self::Native {
                name: _,
                arguments,
                output,
                implementation: _,
            } => Box::new(arguments.iter().chain(output.iter()).cloned()),
        }
    }

    pub fn output(&self) -> Option<Ref<ArgumentDescriptor>> {
        match self {
            Self::Data { output, .. } => Some(output.clone()),
            Self::Native { output, .. } => output.clone(),
        }
    }

    /// Start building a new structure.
    pub fn builder(self: &Ref<Self>) -> structure::StructureBuilder {
        structure::StructureBuilder::new(self.clone())
    }

    /// Produces a goal for the given structure.
    pub fn goal(&self, arguments: structure::Arguments) -> crate::Goal {
        match self {
            StructureDescriptor::Data {
                name,
                inputs,
                output,
            } => {
                let (output_term, arguments) = arguments.without(output);
                let mut term = Term::atom("end");
                for input in inputs.iter().rev() {
                    term = arguments.lookup(input) * term;
                }
                term = Term::atom(name) * term;
                crate::eq(term, output_term)
            }
            StructureDescriptor::Native { implementation, .. } => implementation(arguments),
        }
    }
}

/// A descriptor of an argument of a compound structure.
pub struct ArgumentDescriptor {
    /// The name of the argument.
    pub name: ArgumentName,
    /// The argument descriptor.
    pub descriptor: Descriptor,
}

impl ArgumentDescriptor {
    pub fn output() -> Ref<Self> {
        static OUTPUT: std::sync::LazyLock<Ref<ArgumentDescriptor>> =
            std::sync::LazyLock::new(|| {
                ArgumentDescriptor {
                    name: ArgumentName::of_static("output"),
                    descriptor: Descriptor::Unknown,
                }
                .into()
            });
        OUTPUT.clone()
    }
}

/// The name of an argument of a compound structure.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct ArgumentName(&'static str);

impl std::fmt::Debug for ArgumentName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl ArgumentName {
    /// Construct an argument name from a static string.
    pub const fn of_static(name: &'static str) -> Self {
        Self(name)
    }

    pub fn as_str(&self) -> &str {
        self.0
    }
}

impl From<&'static str> for ArgumentName {
    fn from(value: &'static str) -> Self {
        Self(value)
    }
}
