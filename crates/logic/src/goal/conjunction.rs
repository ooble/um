use super::*;

pub struct Conjunction {
    a: Goal,
    b: Goal,
}

/// Produces the conjunction of two goals, i.e. one that is satisfied if _both_ the underlying
/// goals are satisfied.
pub fn conj(a: Goal, b: Goal) -> Goal {
    Goal::new(Conjunction { a, b })
}

impl GoalImpl for Conjunction {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let b = self.b.clone();
        GoalOutcome::Chain(
            self.a.run_with(context.clone()).into(),
            Arc::new(move |new_context| b.run_with(context.with_goal(new_context))),
        )
    }
}
