use crate::library::Resource;

use super::*;

struct WithResource {
    name: String,
    inner: Box<dyn Fn(Resource) -> Goal + Send + Sync>,
}

pub fn with_resource(
    name: String,
    inner: impl Fn(Resource) -> Goal + Send + Sync + 'static,
) -> Goal {
    Goal::new(WithResource {
        name,
        inner: Box::new(inner),
    })
}

impl GoalImpl for WithResource {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let goal = if let Some(resource) = context.library.lookup(&self.name) {
            (self.inner)(resource)
        } else {
            empty()
        };
        goal.run_with(context).into()
    }
}
