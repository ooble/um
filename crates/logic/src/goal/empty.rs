use super::*;

struct Empty;

/// An empty goal, i.e. one that returns no results.
pub fn empty() -> Goal {
    Goal::new(Empty)
}

impl GoalImpl for Empty {
    fn run_with(&self, _context: Context) -> GoalOutcome {
        GoalOutcome::Failure
    }
}
