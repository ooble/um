use crate::term::Terms;

use super::*;

struct Resolved<T: Terms, F: Fn(T) -> Goal + Send + Sync> {
    terms: T,
    f: F,
}

pub fn resolved<T: Terms>(terms: T, f: impl Fn(T) -> Goal + Send + Sync + 'static) -> Goal {
    Goal::new(Resolved { terms, f })
}

impl<T: Terms, F: Fn(T) -> Goal + Send + Sync> GoalImpl for Resolved<T, F> {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let goal = (self.f)(self.terms.map(|t| t.run(&context.goal.substitution)));
        goal.run_with(context).into()
    }
}
