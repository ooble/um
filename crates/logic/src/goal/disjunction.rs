use super::*;

pub struct Disjunction {
    a: Goal,
    b: Goal,
}

/// Produces the disjuction of two goals, i.e. one that is satisfied if _either_ of the
/// underlying goals are satisfied.
pub fn disj(a: Goal, b: Goal) -> Goal {
    Goal::new(Disjunction { a, b })
}

impl GoalImpl for Disjunction {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let a_outcome = self.a.run_with(context.clone());
        let b_outcome = self.b.run_with(context);
        GoalOutcome::Alternate(a_outcome.into(), [b_outcome].into())
    }
}
