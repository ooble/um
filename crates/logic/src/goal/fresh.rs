use crate::variable::Var;

use super::*;

struct Fresh<F: Fn(Var) -> Goal + Send + Sync> {
    f: F,
}

/// Provides a goal with a new, fresh variable.
pub fn fresh(f: impl Fn(Var) -> Goal + Send + Sync + 'static) -> Goal {
    Goal::new(Fresh { f })
}

impl<F: Fn(Var) -> Goal + Send + Sync> GoalImpl for Fresh<F> {
    fn run_with(&self, mut context: Context) -> GoalOutcome {
        let var = context.goal.next_var();
        let goal = (self.f)(var);
        goal.run_with(context).into()
    }
}

struct FreshN<const N: usize, F: Fn([Var; N]) -> Goal + Send + Sync> {
    f: F,
}

/// Provides a goal with N new, fresh variables.
pub fn fresh_n<const N: usize>(f: impl Fn([Var; N]) -> Goal + Send + Sync + 'static) -> Goal {
    Goal::new(FreshN { f })
}

impl<const N: usize, F: Fn([Var; N]) -> Goal + Send + Sync> GoalImpl for FreshN<N, F> {
    fn run_with(&self, mut context: Context) -> GoalOutcome {
        let vars = context.goal.next_n_vars();
        let goal = (self.f)(vars);
        goal.run_with(context).into()
    }
}
