use std::sync::Arc;

use super::*;

struct Defer<F: Fn() -> Goal + Send + Sync>(Arc<F>);

pub fn defer(f: impl Fn() -> Goal + Send + Sync + 'static) -> Goal {
    Goal::new(Defer(Arc::new(f)))
}

impl<F: Fn() -> Goal + Send + Sync + 'static> GoalImpl for Defer<F> {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let f = Arc::clone(&self.0);
        GoalOutcome::Deferred(Box::new(move || f().run_with(context.clone())))
    }
}
