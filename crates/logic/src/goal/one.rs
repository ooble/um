use super::*;

struct One;

pub fn one() -> Goal {
    Goal::new(One)
}

impl GoalImpl for One {
    fn run_with(&self, context: Context) -> GoalOutcome {
        GoalOutcome::Success(context.goal)
    }
}
