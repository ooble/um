use crate::library::Resource;
use crate::scalar::Scalar;
use crate::term::{Term, TermX};

use super::*;

struct Get {
    value: Term,
    field: Term,
    output: Term,
}

pub fn get(value: Term, field: Term, output: Term) -> Goal {
    Goal::new(Get {
        value,
        field,
        output,
    })
}

impl GoalImpl for Get {
    fn run_with(&self, context: Context) -> GoalOutcome {
        let field = self.field.clone();
        let output = self.output.clone();
        let inner_goal = resolved(self.value.clone(), move |value| {
            let TermX::Product(structure_name_term, _) = value.as_ref() else {
                return empty();
            };

            let TermX::Scalar(Scalar::Atom(structure_name)) = structure_name_term.as_ref() else {
                return empty();
            };

            let value = value.clone();
            let field = field.clone();
            let output = output.clone();
            with_resource(structure_name.clone(), move |resource| {
                let Resource::Structure(descriptor) = resource;

                let TermX::Scalar(Scalar::Atom(field)) = field.as_ref() else {
                    return empty();
                };

                let Some(argument) = descriptor
                    .arguments()
                    .find(|argument| argument.name.as_str() == field.as_str())
                else {
                    return empty();
                };

                let Some(output_descriptor) = descriptor.output() else {
                    return empty();
                };

                descriptor
                    .builder()
                    .with(&argument, output.clone())
                    .with(&output_descriptor, value.clone())
                    .goal()
            })
        });
        inner_goal.run_with(context).into()
    }
}
