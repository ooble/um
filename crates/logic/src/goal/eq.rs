use super::*;
use crate::term::{Term, TermX};

pub struct Eq {
    lhs: Term,
    rhs: Term,
}

/// Binds two terms.
pub fn eq(lhs: impl Into<Term>, rhs: impl Into<Term>) -> Goal {
    Goal::new(Eq {
        lhs: lhs.into(),
        rhs: rhs.into(),
    })
}

impl GoalImpl for Eq {
    fn run_with(&self, context: Context) -> GoalOutcome {
        match unify(&self.lhs, &self.rhs, &context.goal.substitution) {
            None => GoalOutcome::Failure,
            Some(new_substitution) => GoalOutcome::Success(context.goal.with(new_substitution)),
        }
    }
}

/// Unifies two terms after walking them with the given substitution.
fn unify(lhs: &Term, rhs: &Term, substitution: &Substitution) -> Option<Substitution> {
    unify_expr(lhs.walk(substitution), rhs.walk(substitution))
        .and_then(|result| result.merge(substitution))
}

/// Produces a new substitution if the terms unify, or [`None`] if they do not.
fn unify_expr(lhs: Term, rhs: Term) -> Option<Substitution> {
    match (lhs.as_ref(), rhs.as_ref()) {
        (TermX::Ignored, _) | (_, TermX::Ignored) => Some(Substitution::empty()),
        (TermX::Scalar(a), TermX::Scalar(b)) if a == b => Some(Substitution::empty()),
        (TermX::Var(lhs), _) => Some(Substitution::of(*lhs, rhs)),
        (_, TermX::Var(rhs)) => Some(Substitution::of(*rhs, lhs)),
        (TermX::Product(l_a, l_b), TermX::Product(r_a, r_b)) => {
            let a = unify_expr(l_a.clone(), r_a.clone())?;
            let b = unify_expr(l_b.clone(), r_b.clone())?;
            a.merge(&b)
        }
        (TermX::Thunk(lhs), _) => {
            let lhs = lhs.value();
            unify_expr(lhs, rhs)
        }
        (_, TermX::Thunk(rhs)) => {
            let rhs = rhs.value();
            unify_expr(lhs, rhs)
        }
        (_, _) => None,
    }
}
