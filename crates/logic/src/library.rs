use crate::descriptor::*;

#[derive(Clone)]
pub enum Resource {
    Structure(Ref<StructureDescriptor>),
}

impl Resource {
    pub fn name(&self) -> &'static str {
        match self {
            Resource::Structure(ref descriptor) => descriptor.name(),
        }
    }
}

#[derive(Default, Clone)]
pub struct Library {
    index: im::HashMap<&'static str, Resource>,
}

impl Library {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn lookup(&self, name: &str) -> Option<Resource> {
        self.index.get(name).cloned()
    }

    pub fn with(&self, resource: Resource) -> Self {
        Self {
            index: self.index.update(resource.name(), resource),
        }
    }

    pub fn extend(&self, other: Self) -> Self {
        Self {
            index: self.index.clone().union(other.index),
        }
    }
}
