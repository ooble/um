mod conjunction;
mod defer;
mod disjunction;
mod empty;
mod eq;
mod fresh;
mod get;
mod one;
mod resolved;
mod with_resource;

use std::sync::Arc;

use crate::context::{Context, GoalId};
use crate::outcome::Outcome;
use crate::substitution::Substitution;

pub use conjunction::conj;
pub use defer::defer;
pub use disjunction::disj;
pub use empty::empty;
pub use eq::eq;
pub use fresh::{fresh, fresh_n};
pub use get::get;
pub use one::one;
pub use resolved::resolved;
pub use with_resource::with_resource;

pub type GoalOutcome = crate::outcome::OutcomeX;

#[derive(Clone)]
pub struct Goal {
    pub id: GoalId,
    inner: Arc<dyn GoalImpl>,
}

pub trait GoalImpl: Send + Sync {
    /// Runs the goal in context.
    fn run_with(&self, context: Context) -> GoalOutcome;
}

impl Goal {
    pub fn new(goal: impl GoalImpl + 'static) -> Self {
        Self {
            id: GoalId::new_unique(),
            inner: Arc::new(goal),
        }
    }

    pub(crate) fn run_with(&self, context: Context) -> Outcome {
        let outcome = self.inner.run_with(context);
        Outcome {
            goal_id: self.id,
            inner: outcome,
        }
    }
}

impl std::ops::BitAnd for Goal {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        conj(self, rhs)
    }
}

impl std::ops::BitOr for Goal {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        disj(self, rhs)
    }
}
