use std::sync::RwLock;

pub struct Thunk<T>(RwLock<ThunkValue<T>>);

enum ThunkValue<T> {
    Value(T),
    Computation(Box<dyn FnOnce() -> T + Send + Sync>),
    Unfinished,
}

impl<T: Clone> Thunk<T> {
    pub fn new(f: impl FnOnce() -> T + Send + Sync + 'static) -> Self {
        Self(RwLock::new(ThunkValue::Computation(Box::new(f))))
    }

    pub fn value(&self) -> T {
        {
            let lock = self.0.read().unwrap();
            match &*lock {
                // the value already exists
                ThunkValue::Value(value) => return value.clone(),
                // release the lock and proceed
                ThunkValue::Computation(_) => (),
                // something went horribly wrong
                ThunkValue::Unfinished => {
                    unreachable!("thunk never finished, but the lock was released")
                }
            }
        }

        let mut lock = self.0.write().unwrap();
        match &*lock {
            // the value has since been computed
            ThunkValue::Value(value) => value.clone(),
            ThunkValue::Computation(_) => {
                let ThunkValue::Computation(f) =
                    std::mem::replace(&mut *lock, ThunkValue::Unfinished)
                else {
                    unreachable!("Thunk")
                };
                let computed = f();
                *lock = ThunkValue::Value(computed.clone());
                computed
            }
            ThunkValue::Unfinished => {
                unreachable!("thunk never finished, but the lock was released")
            }
        }
    }
}

impl<T> std::fmt::Debug for Thunk<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<thunk>")
    }
}

impl<T> std::cmp::PartialEq for Thunk<T> {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self, other)
    }
}

impl<T> std::cmp::Eq for Thunk<T> {}
