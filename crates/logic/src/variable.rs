/// A variable, to be resolved when searching.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Var(usize);

impl std::fmt::Debug for Var {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{}", self.0)
    }
}

/// Produces fresh, new variables, guaranteed to be unique.
///
/// This guarantee does not hold if you mix and match instances.
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct FreshVariables {
    current: usize,
}

impl FreshVariables {
    /// A new sequence of fresh variables.
    pub fn new() -> Self {
        Self::default()
    }

    /// Produces a new fresh variable and mutates the current sequence.
    #[allow(clippy::should_implement_trait)]
    pub fn next(&mut self) -> Var {
        let var = Var(self.current);
        self.current += 1;
        var
    }

    /// Produces N new fresh variables and mutates the current sequence.
    pub fn next_n<const N: usize>(&mut self) -> [Var; N] {
        let mut vars = [Var(0); N];
        for (i, var) in vars.iter_mut().enumerate() {
            *var = Var(self.current + i);
        }
        self.current += N;
        vars
    }
}
