use std::sync::atomic;

use crate::library::Library;
use crate::substitution::Substitution;
use crate::variable::{FreshVariables, Var};

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct GoalId(pub(crate) u64);

static NEXT_GOAL_ID: atomic::AtomicU64 = atomic::AtomicU64::new(0);

impl GoalId {
    pub fn new_unique() -> Self {
        Self(NEXT_GOAL_ID.fetch_add(1, atomic::Ordering::Relaxed))
    }
}

impl std::fmt::Debug for GoalId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl std::fmt::Display for GoalId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Clone)]
pub struct Context {
    pub library: Library,
    pub goal: GoalContext,
}

impl Context {
    pub fn with_goal(&self, new_goal_context: GoalContext) -> Self {
        Self {
            library: self.library.clone(),
            goal: new_goal_context,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GoalContext {
    pub substitution: Substitution,
    pub fresh: FreshVariables,
}

impl GoalContext {
    pub fn with(self, substitution: Substitution) -> Self {
        Self {
            substitution,
            ..self
        }
    }

    pub fn next_var(&mut self) -> Var {
        self.fresh.next()
    }

    pub fn next_n_vars<const N: usize>(&mut self) -> [Var; N] {
        self.fresh.next_n()
    }
}
