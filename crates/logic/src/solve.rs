use crate::context::{Context, GoalContext};
use crate::goal::Goal;
use crate::library::Library;
use crate::progress::{EventX, Progress};
use crate::substitution::Substitution;
use crate::term::Term;
use crate::variable::{FreshVariables, Var};

/// A solver, which can prepare variables, solve a goal, and then allow for looking up the given
/// variables in the solutions.
#[derive(Default)]
pub struct Solver {
    library: Library,
    fresh: FreshVariables,
}

impl Solver {
    pub fn new(library: Library, fresh: FreshVariables) -> Self {
        Self { library, fresh }
    }

    /// Produces a new variable, which can be used in a goal and then looked up in a solution after
    /// solving.
    pub fn var(&mut self) -> Var {
        self.fresh.next()
    }

    /// Evaluates a goal.
    pub fn evaluate(self, goal: &Goal) -> Progress {
        Progress::new(goal.run_with(Context {
            library: self.library,
            goal: GoalContext {
                substitution: Substitution::empty(),
                fresh: self.fresh,
            },
        }))
    }

    /// Solves a goal.
    pub fn solve(self, goal: &Goal) -> impl Iterator<Item = Solution> {
        let goal_id = goal.id;
        self.evaluate(goal)
            .filter(move |event| event.goal_id == goal_id)
            .filter_map(|event| match event.inner {
                EventX::Success(context) => Some(Solution(context.substitution)),
                EventX::Failure | EventX::Pending => None,
            })
    }
}

/// A single solution to a goal.
pub struct Solution(Substitution);

impl Solution {
    /// Look up a variable in this solution.
    ///
    /// On failure, returns the original variable as a term.
    pub fn lookup(&self, var: Var) -> Term {
        Term::var(var).run(&self.0)
    }
}
