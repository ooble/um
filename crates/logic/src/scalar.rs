#[derive(Debug, PartialEq, Eq)]
pub enum Scalar {
    Atom(String),
    Integer(i64),
    String(String),
    Path(std::path::PathBuf),
}

impl std::fmt::Display for Scalar {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Atom(atom) => write!(f, "{atom}"),
            Self::Integer(integer) => write!(f, "{integer}"),
            Self::String(string) => write!(f, "{string:?}"),
            Self::Path(path) => write!(f, "{}", path.to_string_lossy()),
        }
    }
}
