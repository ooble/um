//! This machine processes outcomes.
//!
//! A goal produces a single [`Outcome`].
//!
//! The [`Progress`] machine iterates that outcome, producing events which can be consumed by the
//! operator.
//!
//! The machine guarantees that every outcome, including nested outcomes, produces at least one
//! event, and that every [`EventX::Pending`] event is matched by a [`EventX::Success`] or
//! [`EventX::Failure`].

use std::collections::VecDeque;

use crate::context::{GoalContext, GoalId};
use crate::outcome::{DeferredOutcome, Next, Outcome, OutcomeX};

/// An event that is emitted by [`Progress`] when processing an [`Outcome`].
#[derive(Debug)]
pub struct Event {
    /// The ID of the goal that produced this event.
    pub goal_id: GoalId,
    /// The value of the event.
    pub inner: EventX,
}

/// The inner event value.
pub enum EventX {
    Success(GoalContext),
    Failure,
    Pending,
}

// Debug formatting is used in testing.
impl std::fmt::Debug for EventX {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                EventX::Success(_) => "Success",
                EventX::Failure => "Failure",
                EventX::Pending => "Pending",
            }
        )
    }
}

impl From<Outcome> for Event {
    fn from(value: Outcome) -> Self {
        Event {
            goal_id: value.goal_id,
            inner: value.inner.into(),
        }
    }
}

impl From<OutcomeX> for EventX {
    fn from(value: OutcomeX) -> Self {
        match value {
            OutcomeX::Success(context) => EventX::Success(context),
            OutcomeX::Failure => EventX::Failure,
            OutcomeX::Deferred { .. }
            | OutcomeX::Chain { .. }
            | OutcomeX::Alternate { .. }
            | OutcomeX::Wrapped { .. } => EventX::Pending,
        }
    }
}

/// The state of the [`Progress`] machine.
struct State {
    /// The ID of the goal that produced this state.
    goal_id: GoalId,
    /// The value of the state.
    inner: StateX,
}

/// The inner state value.
enum StateX {
    /// The conjunction is "done"; a success or failure was produced.
    /// The machine may not be done, as there might be alternatives to pursue.
    Done,
    /// There's a deferred outcome to evaluate.
    Deferred(DeferredOutcome),
    /// An outcome is ready to be processed.
    Outcome(OutcomeX),
}

impl From<Outcome> for State {
    fn from(value: Outcome) -> Self {
        State {
            goal_id: value.goal_id,
            inner: StateX::Outcome(value.inner),
        }
    }
}

/// A handler is used to progress a finished outcome, i.e. a [`OutcomeX::Success`] or
/// [`OutcomeX::Failure`].
#[derive(Clone)]
enum Handler {
    /// Nested outcomes are handled by wrapped handlers.
    Wrapped(GoalId),
    /// Chained outcomes are handled by chain handlers.
    Chain(Next),
}

impl Handler {
    fn handle(self, outcome: Outcome) -> (State, EventX, Option<Handler>) {
        match self {
            Self::Wrapped(goal_id) => {
                let event = match &outcome.inner {
                    OutcomeX::Success(context) => EventX::Success(context.clone()),
                    OutcomeX::Failure => EventX::Failure,
                    OutcomeX::Deferred { .. }
                    | OutcomeX::Chain { .. }
                    | OutcomeX::Alternate { .. }
                    | OutcomeX::Wrapped { .. } => EventX::Pending,
                };
                let state = State {
                    goal_id,
                    inner: StateX::Outcome(outcome.inner),
                };
                (state, event, None)
            }
            Self::Chain(next) => match outcome.inner {
                OutcomeX::Success(context) => {
                    let next_outcome = next(context.clone());
                    let event = EventX::Success(context);
                    (next_outcome.into(), event, None)
                }
                OutcomeX::Failure => (outcome.into(), EventX::Failure, None),
                OutcomeX::Deferred { .. }
                | OutcomeX::Chain { .. }
                | OutcomeX::Alternate { .. }
                | OutcomeX::Wrapped { .. } => (outcome.into(), EventX::Pending, None),
            },
        }
    }
}

/// An iterator which produces events from an outcome.
pub struct Progress {
    /// The current state, which is evaluated.
    state: State,
    /// When a handler is present, it's invoked on success or failure to provide the next state.
    handlers: Vec<Handler>,
    /// When we run out of handlers, we instead proceed to alternatives, queued up as we go.
    alternatives: VecDeque<(State, Vec<Handler>)>,
}

impl Progress {
    pub fn new(outcome: Outcome) -> Self {
        Self {
            state: outcome.into(),
            handlers: Vec::new(),
            alternatives: VecDeque::new(),
        }
    }
}

impl Iterator for Progress {
    type Item = Event;

    fn next(&mut self) -> Option<Self::Item> {
        let goal_id = self.state.goal_id;
        let State { goal_id, inner } = std::mem::replace(
            &mut self.state,
            State {
                goal_id,
                inner: StateX::Done,
            },
        );
        let (next_state, event): (State, EventX) = match inner {
            // When we're done with one branch, see if there's any alternatives.
            StateX::Done => {
                if self.handlers.is_empty() {
                    if let Some((state, handlers)) = self.alternatives.pop_front() {
                        // If so, start them.
                        self.handlers = handlers;
                        (state, EventX::Pending)
                    } else {
                        // If not, we're finished.
                        return None;
                    }
                } else {
                    // If there are handlers left, we reached this state by accident.
                    // It's probably best just to fail hard.
                    unreachable!("Completed iteration with handlers left.");
                }
            }
            // Run deferred operations on demand.
            // This is the second step of evaluating a deferred operation.
            // It is split so that we can return `Pending` before it starts.
            StateX::Deferred(deferred) => (deferred().into(), EventX::Pending),
            // Success and Failure are final states, and get passed to a handler if there is one.
            StateX::Outcome(inner @ (OutcomeX::Success(_) | OutcomeX::Failure)) => {
                if let Some(handler) = self.handlers.pop() {
                    // If there is a handler, run it.
                    let (next_state, next_event, next_handler) =
                        handler.handle(Outcome { goal_id, inner });
                    if let Some(next_handler) = next_handler {
                        self.handlers.push(next_handler);
                    }
                    (next_state, next_event)
                } else {
                    // On success or failure without a handler, we're `Done`.
                    (
                        State {
                            goal_id,
                            inner: StateX::Done,
                        },
                        inner.into(),
                    )
                }
            }
            // If we need to evaluate a deferred operation, get it ready, but don't handle it yet.
            StateX::Outcome(OutcomeX::Deferred(deferred)) => {
                self.handlers.push(Handler::Wrapped(goal_id));
                (
                    State {
                        goal_id,
                        inner: StateX::Deferred(deferred),
                    },
                    EventX::Pending,
                )
            }
            // Chain as we go for now.
            StateX::Outcome(OutcomeX::Chain(current, next)) => {
                self.handlers.push(Handler::Wrapped(goal_id));
                self.handlers.push(Handler::Chain(next));
                ((*current).into(), EventX::Pending)
            }
            // Alternatives get pushed onto a separate queue.
            StateX::Outcome(OutcomeX::Alternate(first, mut rest)) => {
                if let Some(second) = rest.pop_front() {
                    let alternative = State {
                        goal_id,
                        inner: StateX::Outcome(OutcomeX::Alternate(second.into(), rest)),
                    };
                    self.alternatives
                        .push_back((alternative, self.handlers.clone()));
                }
                self.handlers.push(Handler::Wrapped(goal_id));
                ((*first).into(), EventX::Pending)
            }
            // Sometimes, we wrap on purpose. We push this to a handler so that we ensure we emit
            // a final event with the correct goal ID.
            StateX::Outcome(OutcomeX::Wrapped(wrapped)) => {
                self.handlers.push(Handler::Wrapped(goal_id));
                ((*wrapped).into(), EventX::Pending)
            }
        };
        self.state = next_state;
        Some(Event {
            goal_id,
            inner: event,
        })
    }
}

impl std::iter::FusedIterator for Progress {}

#[cfg(test)]
mod tests {
    use crate::goal::*;
    use crate::solve::Solver;
    use crate::term::Term;

    #[allow(clippy::too_many_lines)]
    #[test]
    fn emits_events() {
        let solver = Solver::default();
        let goal =
            // goal 0
            fresh(|v| {
                // goal 1
                fresh(move |w| {
                    eq(v, Term::atom("1")) // goal 2
                    & // goal 6
                    (
                        eq(v, Term::atom("2")) // goal 3
                        | // goal 5
                        eq(v, Term::atom("1")) // goal 4
                    )
                    & // goal 8
                    eq(w, Term::atom("3")) // goal 7
                })
            });

        let results: Vec<_> = solver.evaluate(&goal).collect();

        insta::assert_debug_snapshot!(results, @r#"
        [
            Event {
                goal_id: 0,
                inner: Pending,
            },
            Event {
                goal_id: 1,
                inner: Pending,
            },
            Event {
                goal_id: 8,
                inner: Pending,
            },
            Event {
                goal_id: 6,
                inner: Pending,
            },
            Event {
                goal_id: 2,
                inner: Success,
            },
            Event {
                goal_id: 5,
                inner: Pending,
            },
            Event {
                goal_id: 3,
                inner: Failure,
            },
            Event {
                goal_id: 5,
                inner: Failure,
            },
            Event {
                goal_id: 6,
                inner: Failure,
            },
            Event {
                goal_id: 6,
                inner: Failure,
            },
            Event {
                goal_id: 8,
                inner: Failure,
            },
            Event {
                goal_id: 1,
                inner: Failure,
            },
            Event {
                goal_id: 0,
                inner: Failure,
            },
            Event {
                goal_id: 0,
                inner: Pending,
            },
            Event {
                goal_id: 5,
                inner: Pending,
            },
            Event {
                goal_id: 4,
                inner: Success,
            },
            Event {
                goal_id: 5,
                inner: Success,
            },
            Event {
                goal_id: 6,
                inner: Success,
            },
            Event {
                goal_id: 7,
                inner: Success,
            },
            Event {
                goal_id: 8,
                inner: Success,
            },
            Event {
                goal_id: 1,
                inner: Success,
            },
            Event {
                goal_id: 0,
                inner: Success,
            },
        ]
        "#
        );
    }
}
