use std::error::Error;
use std::sync::Arc;

use crate::scalar::Scalar;
use crate::substitution::Substitution;
use crate::thunk::Thunk;
use crate::variable::Var;

/// A term.
///
/// This is cheaply cloneable.
#[derive(Clone, PartialEq, Eq)]
pub struct Term(Arc<TermX>);

impl std::fmt::Debug for Term {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// The innards of a term.
#[derive(Debug, PartialEq, Eq)]
pub enum TermX {
    Ignored,
    Error(String),
    Var(Var),
    Scalar(Scalar),
    Product(Term, Term),
    Thunk(Thunk<Term>),
}

impl Term {
    /// Substitute all variables in a term with the given substitutions.
    pub(crate) fn walk(&self, substitution: &Substitution) -> Self {
        match self.as_ref() {
            TermX::Var(v) => match substitution.lookup(*v) {
                Some(next) => next.walk(substitution),
                None => self.clone(),
            },
            TermX::Product(a, b) => Self::product(a.walk(substitution), b.walk(substitution)),
            TermX::Ignored | TermX::Error(_) | TermX::Scalar(_) | TermX::Thunk(_) => self.clone(),
        }
    }

    /// Substitute all variables and follow all thunks in a term, using the given substitutions.
    pub(crate) fn run(&self, substitution: &Substitution) -> Term {
        let walked = self.walk(substitution);
        if let TermX::Thunk(thunk) = walked.as_ref() {
            thunk.value().walk(substitution)
        } else {
            walked
        }
    }

    pub fn ignored() -> Self {
        Self::from(TermX::Ignored)
    }

    pub fn error(error: String) -> Self {
        Self::from(TermX::Error(error))
    }

    pub fn var(var: Var) -> Self {
        Self::from(TermX::Var(var))
    }

    pub fn atom(name: &str) -> Self {
        Self::from(TermX::Scalar(Scalar::Atom(name.to_string())))
    }

    pub fn int(value: i64) -> Self {
        Self::from(TermX::Scalar(Scalar::Integer(value)))
    }

    pub fn string(value: impl Into<String>) -> Self {
        Self::from(TermX::Scalar(Scalar::String(value.into())))
    }

    pub fn scalar(scalar: Scalar) -> Self {
        Self::from(TermX::Scalar(scalar))
    }

    pub fn product(a: Term, b: Term) -> Self {
        Self::from(TermX::Product(a, b))
    }

    pub fn thunk(f: impl FnOnce() -> Result<Term, Box<dyn Error>> + Send + Sync + 'static) -> Self {
        Self::from(TermX::Thunk(Thunk::new(|| {
            f().unwrap_or_else(|error| Term::error(error.to_string()))
        })))
    }
}

impl std::ops::Mul for Term {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Term::product(self, rhs)
    }
}

impl std::ops::Mul for &Term {
    type Output = Term;

    fn mul(self, rhs: Self) -> Self::Output {
        self.clone() * rhs.clone()
    }
}

impl From<&Term> for Term {
    fn from(value: &Term) -> Self {
        value.clone()
    }
}

impl From<TermX> for Term {
    fn from(value: TermX) -> Self {
        Self(Arc::new(value))
    }
}

impl AsRef<TermX> for Term {
    fn as_ref(&self) -> &TermX {
        self.0.as_ref()
    }
}

impl From<Var> for Term {
    fn from(value: Var) -> Self {
        Self::var(value)
    }
}

pub trait Terms: Send + Sync + 'static {
    fn map(&self, f: impl Fn(Term) -> Term) -> Self;
}

impl Terms for Term {
    fn map(&self, f: impl Fn(Term) -> Term) -> Self {
        f(self.clone())
    }
}

impl<const N: usize> Terms for [Term; N] {
    fn map(&self, f: impl Fn(Term) -> Term) -> Self {
        <[Term; N]>::map(self.clone(), f)
    }
}
