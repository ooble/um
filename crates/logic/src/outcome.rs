//! A [`Goal`][crate::goal::Goal] produces an [`Outcome`].

use std::collections::VecDeque;
use std::sync::Arc;

use crate::context::{GoalContext, GoalId};

/// An outcome is produced as a result of evaluating a goal.
pub struct Outcome {
    /// The ID of the goal that produced this outcome.
    pub(crate) goal_id: GoalId,
    /// The value of the outcome.
    pub(crate) inner: OutcomeX,
}

/// The inner outcome value.
pub enum OutcomeX {
    /// A successful outcome, which produces a context that can be provided to
    /// another goal.
    Success(GoalContext),
    /// A failed outcome. No context is provided.
    Failure,
    /// A deferred outcome, which will produce an outcome when evaluated.
    Deferred(DeferredOutcome),
    /// A chained outcome, which consists of an outcome and a way to transform it on success.
    Chain(Box<Outcome>, Next),
    /// An non-empty list of alternative outcomes.
    Alternate(Box<Outcome>, VecDeque<Outcome>),
    /// A wrapped outcome, used to ensure that a goal ID is propagated.
    Wrapped(Box<Outcome>),
}

/// A deferred outcome, which will produce an outcome when evaluated.
pub type DeferredOutcome = Box<dyn FnOnce() -> Outcome>;

/// A function which takes as input the context from the previous goal.
pub type Next = Arc<dyn Fn(GoalContext) -> Outcome>;

impl From<Outcome> for OutcomeX {
    fn from(value: Outcome) -> Self {
        Self::Wrapped(Box::new(value))
    }
}
