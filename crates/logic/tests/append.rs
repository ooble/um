use std::collections::VecDeque;

use um_logic::*;

fn terminator() -> Term {
    Term::atom("terminator")
}

fn list_to_product(mut terms: VecDeque<Term>) -> Term {
    if let Some(first) = terms.pop_front() {
        first * list_to_product(terms)
    } else {
        terminator()
    }
}

// The miniKanren definition is as follows:
// (define append
//   (lambda (a b out)
//     (conde
//       ((≡ '() a) (≡ b out))
//       ((fresh (a_head a_tail out_tail)
//         (≡ ((a_head . a_tail)) a)
//         (≡ ((a_head . out_tail)) out)
//         (append a_tail b out_tail))))))
fn append(a: Term, b: Term, out: Term) -> Goal {
    eq(terminator(), &a) & eq(&b, &out)
        | fresh_n(move |[a_head, a_tail, out_tail]| {
            eq(Term::var(a_head) * Term::var(a_tail), &a)
                & eq(Term::var(a_head) * Term::var(out_tail), &out)
                & append(a_tail.into(), b.clone(), out_tail.into())
        })
}

#[test]
fn unifies_append_output() {
    let mut solver = Solver::default();
    let output = solver.var();
    let goal = append(
        list_to_product([Term::atom("a"), Term::atom("b")].into()),
        list_to_product([Term::atom("c")].into()),
        Term::var(output),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(
        results[0].lookup(output),
        list_to_product([Term::atom("a"), Term::atom("b"), Term::atom("c")].into()),
    );
}

#[test]
fn unifies_append_input() {
    let mut solver = Solver::default();
    let input = solver.var();
    let goal = append(
        list_to_product([Term::atom("a")].into()),
        Term::var(input),
        list_to_product([Term::atom("a"), Term::atom("b"), Term::atom("c")].into()),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(
        results[0].lookup(input),
        list_to_product([Term::atom("b"), Term::atom("c")].into()),
    );
}

#[test]
fn unifies_part_of_append_input() {
    let mut solver = Solver::default();
    let input = solver.var();
    let goal = append(
        list_to_product([Term::atom("a")].into()),
        list_to_product([Term::atom("b"), Term::var(input)].into()),
        list_to_product([Term::atom("a"), Term::atom("b"), Term::atom("c")].into()),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(input), Term::atom("c"));
}
