use std::collections::VecDeque;

use um_logic::*;

#[test]
fn solves_disjunctions() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = eq(v, Term::int(1)) | eq(v, Term::int(2));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 2);
    assert_eq!(results[0].lookup(v), Term::int(1));
    assert_eq!(results[1].lookup(v), Term::int(2));
}

#[test]
fn solves_conjunctions_and_disjunctions() {
    let mut solver = Solver::default();
    let v = solver.var();
    let w = solver.var();
    let x = solver.var();
    let goal = eq(v, Term::atom("7"))
        & (eq(w, Term::atom("5")) | eq(w, Term::atom("6")))
        & eq(x, Term::atom("9"));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 2);
    assert_eq!(results[0].lookup(v), Term::atom("7"));
    assert_eq!(results[0].lookup(w), Term::atom("5"));
    assert_eq!(results[0].lookup(x), Term::atom("9"));
    assert_eq!(results[1].lookup(v), Term::atom("7"));
    assert_eq!(results[1].lookup(w), Term::atom("6"));
    assert_eq!(results[1].lookup(x), Term::atom("9"));
}

#[test]
fn disjunctions_alternate() {
    let odds = |v| numbers(v, [1, 3, 5, 7, 9].into());
    let evens = |v| numbers(v, [2, 4, 6, 8, 10].into());

    let mut solver = Solver::default();
    let v = solver.var();
    let goal = odds(v) | evens(v);

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 10);
    (0..10).for_each(|i| {
        assert_eq!(results[i].lookup(v), Term::int((i + 1).try_into().unwrap()));
    });
}

#[test]
fn respects_deferred_operations() {
    let odds = |v| sequence(v, 1, |x| x + 2);
    let evens = |v| sequence(v, 2, |x| x + 2);

    let mut solver = Solver::default();
    let v = solver.var();
    let goal = odds(v) | evens(v);

    let results: Vec<Solution> = solver.solve(&goal).take(5).collect();

    assert_eq!(results.len(), 5);
    (0..5).for_each(|i| {
        assert_eq!(results[i].lookup(v), Term::int((i + 1).try_into().unwrap()));
    });
}

fn numbers(var: Var, mut values: VecDeque<i64>) -> Goal {
    if let Some(first) = values.pop_front() {
        eq(var, Term::int(first)) | numbers(var, values.clone())
    } else {
        empty()
    }
}

fn sequence(var: Var, current: i64, next: fn(i64) -> i64) -> Goal {
    eq(var, Term::int(current)) | defer(move || sequence(var, next(current), next))
}
