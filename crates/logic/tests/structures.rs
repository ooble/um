use std::io::Write;
use std::io::{self, Read};
use std::process::{Command, Stdio};

use um_logic::descriptor::*;
use um_logic::library::{Library, Resource};
use um_logic::structure::*;
use um_logic::*;

#[test]
fn relates_arbitrary_data() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = plus::DESCRIPTOR
        .builder()
        .set(&plus::LEFT, Scalar::Integer(3))
        .set(&plus::RIGHT, Scalar::Integer(2))
        .var(&plus::OUTPUT, v)
        .goal();

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::int(5));
}

#[test]
fn rejects_invalid_cases() {
    let solver = Solver::default();
    let goal = plus::DESCRIPTOR
        .builder()
        .set(&plus::LEFT, Scalar::Integer(3))
        .set(&plus::RIGHT, Scalar::Integer(2))
        .set(&plus::OUTPUT, Scalar::Integer(9))
        .goal();

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn runs_arbitrary_computations_to_compute_structures() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = lines::DESCRIPTOR
        .builder()
        .set(&lines::COMMAND, Scalar::String("sort".to_owned()))
        .set(
            &lines::INPUT,
            Scalar::String(["one", "two", "three", "four", "five"].join("\n") + "\n"),
        )
        .var(&lines::OUTPUT, v)
        .goal();

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(
        results[0].lookup(v),
        Term::string(["five", "four", "one", "three", "two"].join("\n") + "\n")
    );
}

#[test]
fn walks_through_substitutions() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = fresh(move |w| {
        eq(w, Term::string("wc"))
            & lines::DESCRIPTOR
                .builder()
                .var(&lines::COMMAND, w)
                .set(
                    &lines::INPUT,
                    Scalar::String(["one", "two", "three"].join("\n") + "\n"),
                )
                .var(&lines::OUTPUT, v)
                .goal()
    });

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(
        results[0].lookup(v),
        Term::string("      3       3      14\n")
    );
}

#[test]
fn resolves_thunks_when_asked() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = lines::DESCRIPTOR
        .builder()
        .with(&lines::COMMAND, Term::thunk(|| Ok(Term::string("cat"))))
        .set(&lines::INPUT, Scalar::String("abc\n".to_owned()))
        .var(&lines::OUTPUT, v)
        .goal();

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::string("abc\n".to_owned()));
}

#[test]
fn accesses_values_in_data_structures() {
    let mut solver = Solver::new(
        Library::new().with(Resource::Structure(duo::DESCRIPTOR.clone())),
        FreshVariables::new(),
    );
    let duo = solver.var();
    let artist = solver.var();
    let goal = conj(
        duo::DESCRIPTOR
            .builder()
            .set(&duo::LEFT, Scalar::String("Paul".to_owned()))
            .set(&duo::RIGHT, Scalar::String("Storm".to_owned()))
            .var(&ArgumentDescriptor::output(), duo)
            .goal(),
        get(duo.into(), Term::atom("right"), artist.into()),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(artist), Term::string("Storm".to_owned()));
}

mod plus {
    use super::*;

    lazy_static! {
        pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
            name: "test/plus",
            arguments: vec![LEFT.clone(), RIGHT.clone()],
            output: Some(OUTPUT.clone()),
            implementation: goal,
        }
        .into();
        pub(super) static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("left"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
        }
        .into();
        pub(super) static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("right"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
        }
        .into();
        pub(super) static ref OUTPUT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("output"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
        }
        .into();
    }

    fn goal(values: Arguments) -> Goal {
        resolved(
            [values.lookup(&LEFT), values.lookup(&RIGHT)],
            move |[left, right]| match (left.as_ref(), right.as_ref()) {
                (TermX::Scalar(Scalar::Integer(left)), TermX::Scalar(Scalar::Integer(right))) => {
                    eq(values.lookup(&OUTPUT), Term::int(left + right))
                }
                _ => todo!("plus"),
            },
        )
    }
}

mod lines {
    use super::*;

    lazy_static! {
        pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
            name: "test/lines",
            arguments: vec![COMMAND.clone(), INPUT.clone()],
            output: Some(OUTPUT.clone()),
            implementation: goal,
        }
        .into();
        pub(super) static ref COMMAND: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("command"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::String),
        }
        .into();
        pub(super) static ref INPUT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("input"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::String),
        }
        .into();
        pub(super) static ref OUTPUT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("output"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::String),
        }
        .into();
    }

    fn goal(values: Arguments) -> Goal {
        resolved(
            [values.lookup(&COMMAND), values.lookup(&INPUT)],
            move |[command, input]| eq(values.lookup(&OUTPUT), Term::thunk(|| run(command, input))),
        )
    }

    #[allow(clippy::needless_pass_by_value)]
    fn run(command_term: Term, input_term: Term) -> Result<Term, Box<dyn std::error::Error>> {
        let TermX::Scalar(Scalar::String(command_str)) = command_term.as_ref() else {
            return Err("lines: no command".into());
        };

        let TermX::Scalar(Scalar::String(input_str)) = input_term.as_ref() else {
            return Err("lines: no input".into());
        };

        let mut process = Command::new(command_str)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()?;
        {
            let mut stdin = process
                .stdin
                .take()
                .ok_or(io::Error::new(io::ErrorKind::BrokenPipe, "no STDIN"))?;
            write!(stdin, "{input_str}")?;
        }
        let mut output = String::new();
        {
            let mut stdout = process
                .stdout
                .take()
                .ok_or(io::Error::new(io::ErrorKind::BrokenPipe, "no STDOUT"))?;
            stdout.read_to_string(&mut output)?;
        };
        Ok(Term::string(output))
    }
}

mod duo {
    use um_logic::lazy_static;

    use super::*;

    lazy_static! {
        pub(super) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Data {
            name: "test/duo",
            inputs: vec![LEFT.clone(), RIGHT.clone()],
            output: ArgumentDescriptor::output(),
        }
        .into();
        pub(super) static ref LEFT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("left"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::String),
        }
        .into();
        pub(super) static ref RIGHT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
            name: ArgumentName::of_static("right"),
            descriptor: Descriptor::Scalar(ScalarDescriptor::String),
        }
        .into();
    }
}
