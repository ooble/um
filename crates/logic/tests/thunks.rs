use std::sync::atomic::{AtomicI64, Ordering};
use std::sync::Arc;

use um_logic::*;

#[test]
fn invokes_thunks_to_solve_them() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = eq(Term::thunk(|| Ok(Term::int(9))), v);

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::int(9));
}

#[test]
fn rejects_equality_based_on_thunk_values() {
    let solver = Solver::default();
    let goal = eq(
        Term::thunk(|| Ok(Term::int(1))),
        Term::thunk(|| Ok(Term::int(2))),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn invokes_thunks_nested_in_products() {
    let counter = Arc::new(AtomicI64::new(0));
    let counter_ = Arc::clone(&counter);
    let inc = move || {
        Ok(Term::int(
            Arc::clone(&counter_).fetch_add(1, Ordering::Relaxed),
        ))
    };
    let mut solver = Solver::default();
    let v = solver.var();
    let w = solver.var();
    let goal = eq(
        Term::thunk(inc.clone()) * Term::thunk(inc),
        Term::var(v) * Term::var(w),
    );

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::int(0));
    assert_eq!(results[0].lookup(w), Term::int(1));
    assert_eq!(counter.load(Ordering::Relaxed), 2);
}

#[test]
fn does_not_invoke_ignored_thunks() {
    let counter = Arc::new(AtomicI64::new(0));
    let counter_ = Arc::clone(&counter);
    let inc = move || {
        Ok(Term::int(
            Arc::clone(&counter_).fetch_add(1, Ordering::Relaxed),
        ))
    };
    let solver = Solver::default();
    let goal = eq(Term::thunk(inc), Term::ignored());

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(counter.load(Ordering::Relaxed), 0);
}

#[test]
fn only_invokes_an_thunk_once_even_when_it_is_used_twice() {
    let counter = Arc::new(AtomicI64::new(0));
    let counter_ = Arc::clone(&counter);
    let inc = move || {
        Ok(Term::int(
            Arc::clone(&counter_).fetch_add(1, Ordering::Relaxed),
        ))
    };
    let solver = Solver::default();
    let goal = fresh(move |v| eq(Term::thunk(inc.clone()), v) & eq(v, Term::int(0)));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(counter.load(Ordering::Relaxed), 1);
}

#[test]
fn propagates_errors() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = eq(Term::thunk(|| Err("oh no".into())), v);

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::error("oh no".to_owned()));
}
