use um_logic::*;

#[test]
fn solves_for_equality() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = eq(v, Term::atom("result"));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::atom("result"));
}
