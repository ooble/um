use um_logic::*;

#[test]
fn solves_products() {
    let mut solver = Solver::default();
    let v = solver.var();
    let w = solver.var();
    let goal = eq(Term::var(v) * Term::int(3), Term::int(4) * Term::var(w));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::int(4));
    assert_eq!(results[0].lookup(w), Term::int(3));
}

#[test]
fn rejects_products_where_neither_item_unifies() {
    let solver = Solver::default();
    let goal = eq(Term::int(1) * Term::int(2), Term::int(3) * Term::int(4));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn rejects_products_where_the_left_item_does_not_unify() {
    let solver = Solver::default();
    let goal = eq(Term::int(1) * Term::int(2), Term::int(3) * Term::int(2));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn rejects_products_where_the_right_item_does_not_unify() {
    let solver = Solver::default();
    let goal = eq(Term::int(1) * Term::int(2), Term::int(1) * Term::int(4));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn rejects_products_which_produce_mismatching_substitutions() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = eq(Term::var(v) * Term::var(v), Term::int(1) * Term::int(2));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}
