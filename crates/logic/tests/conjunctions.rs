use um_logic::*;

#[test]
fn solves_conjunctions() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = fresh(move |w| eq(v, w) & eq(w, Term::atom("solved")));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::atom("solved"));
}

#[test]
fn rejects_conflicting_conjunctions() {
    let solver = Solver::default();
    let goal = fresh(|v| eq(v, Term::int(1)) & eq(v, Term::int(2)));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 0);
}

#[test]
fn solves_conjunctions_by_evaluating_deferred_operations() {
    let mut solver = Solver::default();
    let v = solver.var();
    let goal = fresh(move |w| defer(move || eq(v, w)) & defer(move || eq(w, Term::atom("solved"))));

    let results: Vec<Solution> = solver.solve(&goal).collect();

    assert_eq!(results.len(), 1);
    assert_eq!(results[0].lookup(v), Term::atom("solved"));
}
