This crate defines the logical system used by the rest of Um.

It is based on µKanren, "a minimal functional core for relational programming",
as described in the paper: http://webyrd.net/scheme-2013/papers/HemannMuKanren2013.pdf

µKanren implements a simple, complete, logic system based on unification.
