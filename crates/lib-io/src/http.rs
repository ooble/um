use std::sync::OnceLock;

use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

static CLIENT: OnceLock<reqwest::blocking::Client> = OnceLock::new();

fn client() -> reqwest::blocking::Client {
    CLIENT.get_or_init(reqwest::blocking::Client::new).clone()
}

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "io/http",
        arguments: vec![URL.clone()],
        output: Some(RESPONSE.clone()),
        implementation: goal,
    }
    .into();
    static ref URL: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("url"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::String),
    }
    .into();
    static ref RESPONSE: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("response"),
        descriptor: Descriptor::Structure(RESPONSE_DESCRIPTOR.clone()),
    }
    .into();
    pub(crate) static ref RESPONSE_DESCRIPTOR: Ref<StructureDescriptor> =
        StructureDescriptor::Data {
            name: "io/http/response",
            inputs: vec![STATUS.clone(), BODY.clone()],
            output: ArgumentDescriptor::output(),
        }
        .into();
    static ref STATUS: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("status"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
    static ref BODY: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("body"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::String),
    }
    .into();
}

#[allow(clippy::needless_pass_by_value)]
fn goal(values: Arguments) -> Goal {
    let response_output = values.lookup(&RESPONSE);
    resolved(values.lookup(&URL), move |url_input| {
        let response_output = response_output.clone();
        defer(move || {
            let TermX::Scalar(Scalar::String(url)) = url_input.as_ref() else {
                return empty();
            };

            let Ok(url) = reqwest::Url::parse(url) else {
                return empty();
            };
            let Ok(response) = client().get(url).send() else {
                return empty();
            };
            let status = response.status().as_u16();
            let Ok(body) = response.text() else {
                return empty();
            };

            RESPONSE_DESCRIPTOR
                .builder()
                .set(&STATUS, Scalar::Integer(status.into()))
                .set(&BODY, Scalar::String(body))
                .with(&ArgumentDescriptor::output(), response_output.clone())
                .goal()
        })
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn make_a_get_request() {
        let mut http_server = mockito::Server::new();
        let url = http_server.url();
        let _mock = http_server
            .mock("GET", "/knock")
            .with_body("Hello, new friend!")
            .create();

        let mut solver = Solver::default();
        let body = solver.var();
        // http[url = "{url}/knock", response = response[status = 200, body = $body]]
        let goal = DESCRIPTOR
            .builder()
            .set(&URL, Scalar::String(format!("{url}/knock")))
            .nest(
                RESPONSE.clone(),
                RESPONSE_DESCRIPTOR
                    .builder()
                    .set(&STATUS, Scalar::Integer(200))
                    .var(&BODY, body),
            )
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);
        assert_eq!(results[0].lookup(body), Term::string("Hello, new friend!"));
    }

    #[test]
    fn fail_if_the_status_is_wrong() {
        let mut http_server = mockito::Server::new();
        let url = http_server.url();
        let _mock = http_server.mock("GET", "/").with_body("Enter.").create();

        let solver = Solver::default();
        // http[url = "{url}", response = response[status = 301]]
        let goal = DESCRIPTOR
            .builder()
            .set(&URL, Scalar::String(url))
            .nest(
                RESPONSE.clone(),
                RESPONSE_DESCRIPTOR
                    .builder()
                    .set(&STATUS, Scalar::Integer(301)),
            )
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 0);
    }
}
