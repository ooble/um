mod file;
mod http;
mod tcp;

use um_logic::library::{Library, Resource};

pub fn library() -> Library {
    Library::new()
        .with(Resource::Structure(file::DESCRIPTOR.clone()))
        .with(Resource::Structure(http::DESCRIPTOR.clone()))
        .with(Resource::Structure(http::RESPONSE_DESCRIPTOR.clone()))
        .with(Resource::Structure(tcp::DESCRIPTOR.clone()))
}
