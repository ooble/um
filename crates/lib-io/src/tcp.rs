use std::net::TcpStream;

use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "io/tcp",
        arguments: vec![HOST.clone(), PORT.clone()],
        output: None,
        implementation: goal,
    }
    .into();
    static ref HOST: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("host"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::String),
    }
    .into();
    static ref PORT: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("port"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
}

#[allow(clippy::needless_pass_by_value)]
fn goal(values: Arguments) -> Goal {
    let input = [values.lookup(&HOST), values.lookup(&PORT)];
    resolved(input, |[host_input, port_input]| {
        let port_expected = port_input.clone();
        let port_actual = Term::thunk(move || {
            let TermX::Scalar(Scalar::String(host)) = host_input.as_ref() else {
                return Err("Tcp: no host".into());
            };
            let TermX::Scalar(Scalar::Integer(port)) = port_input.as_ref() else {
                return Err("Tcp: no port".into());
            };
            let stream = TcpStream::connect((host.as_ref(), u16::try_from(*port)?))?;
            let addr = stream.peer_addr()?;
            Ok(Term::int(i64::from(addr.port())))
        });
        eq(port_expected, port_actual)
    })
}

#[cfg(test)]
mod tests {
    use std::net::*;
    use std::thread;

    use super::*;

    #[test]
    fn socket_is_open() -> anyhow::Result<()> {
        let listener = TcpListener::bind((IpAddr::V6(Ipv6Addr::LOCALHOST), 0))?;
        let address = listener.local_addr()?;
        let listening_thread = thread::spawn(move || {
            // wait for a single connection, then stop
            let _ = listener.incoming().next().unwrap();
        });

        let solver = Solver::default();
        // tcp[host = <address.host>, port = <address.port>]
        let goal = DESCRIPTOR
            .builder()
            .set(&HOST, Scalar::String(address.ip().to_string()))
            .set(&PORT, Scalar::Integer(address.port().into()))
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);

        listening_thread.join().unwrap();
        Ok(())
    }

    #[test]
    fn socket_is_closed() {
        let solver = Solver::default();
        // tcp[host = "localhost", port = 1234]
        let goal = DESCRIPTOR
            .builder()
            .set(&HOST, Scalar::String("localhost".to_string()))
            .set(&PORT, Scalar::Integer(1234.into()))
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 0);
    }
}
