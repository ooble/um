use std::fs;

use um_logic::descriptor::*;
use um_logic::structure::*;
use um_logic::*;

lazy_static! {
    pub(crate) static ref DESCRIPTOR: Ref<StructureDescriptor> = StructureDescriptor::Native {
        name: "io/file",
        arguments: vec![PATH.clone(), SIZE.clone(), CONTENTS.clone()],
        output: None,
        implementation: goal,
    }
    .into();
    static ref PATH: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("path"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Path),
    }
    .into();
    static ref SIZE: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("size"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::Integer),
    }
    .into();
    static ref CONTENTS: Ref<ArgumentDescriptor> = ArgumentDescriptor {
        name: ArgumentName::of_static("contents"),
        descriptor: Descriptor::Scalar(ScalarDescriptor::String),
    }
    .into();
}

fn goal(values: Arguments) -> Goal {
    let input = values.lookup(&PATH);
    resolved(input, move |path_input| match path_input.as_ref() {
        TermX::Scalar(Scalar::Path(path)) => match fs::metadata(path) {
            Ok(metadata) => {
                let path = path.to_owned();
                let size = Term::scalar(Scalar::Integer(metadata.len().try_into().unwrap()));
                let contents = Term::thunk(move || {
                    let contents = fs::read_to_string(path)?;
                    Ok(Term::scalar(Scalar::String(contents)))
                });
                eq(
                    values.lookup(&SIZE) * values.lookup(&CONTENTS),
                    size * contents,
                )
            }
            Err(_) => empty(),
        },
        _ => empty(),
    })
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::PathBuf;

    use super::*;

    #[test]
    fn file_exists() -> anyhow::Result<()> {
        let dir = tempfile::tempdir()?;
        let path = dir.path().join("test.txt");
        fs::write(&path, "Hello, world!")?;

        let solver = Solver::default();
        // file[path = <path>]
        let goal = DESCRIPTOR.builder().set(&PATH, Scalar::Path(path)).goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);
        Ok(())
    }

    #[test]
    fn file_does_not_exist() {
        let path = PathBuf::from("/tmp/does-not-exist");

        let solver = Solver::default();
        // file[path = <path>]
        let goal = DESCRIPTOR.builder().set(&PATH, Scalar::Path(path)).goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 0);
    }

    #[test]
    fn captures_additional_file_attributes() -> anyhow::Result<()> {
        let dir = tempfile::tempdir()?;
        let path = dir.path().join("test.txt");
        fs::write(&path, "this is a file containing exactly 42 bytes")?;

        let mut solver = Solver::default();
        let size = solver.var();
        // file[path = <path>, size = <size>]
        let goal = DESCRIPTOR
            .builder()
            .set(&PATH, Scalar::Path(path))
            .var(&SIZE, size)
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);
        assert_eq!(
            results[0].lookup(size),
            Term::scalar(Scalar::Integer(42.into()))
        );
        Ok(())
    }

    #[test]
    fn captures_file_contents() -> anyhow::Result<()> {
        let dir = tempfile::tempdir()?;
        let path = dir.path().join("test.txt");
        fs::write(&path, "much content, very good")?;

        let mut solver = Solver::default();
        let contents = solver.var();
        // file[path = <path>, size = <size>, contents = <contents>]
        let goal = DESCRIPTOR
            .builder()
            .set(&PATH, Scalar::Path(path))
            .var(&CONTENTS, contents)
            .goal();
        let results: Vec<Solution> = solver.solve(&goal).collect();

        assert_eq!(results.len(), 1);
        assert_eq!(
            results[0].lookup(contents),
            Term::scalar(Scalar::String("much content, very good".to_string()))
        );
        Ok(())
    }

    #[test]
    fn asserts_file_contents() -> anyhow::Result<()> {
        let dir = tempfile::tempdir()?;
        let path = dir.path().join("test.txt");
        fs::write(&path, "hi there")?;

        let results_matched: Vec<Solution> = {
            let solver = Solver::default();
            // file[path = <path>, size = <size>, contents = "hi there"]
            let goal = DESCRIPTOR
                .builder()
                .set(&PATH, Scalar::Path(path.clone()))
                .set(&CONTENTS, Scalar::String("hi there".to_string()))
                .goal();
            solver.solve(&goal).collect()
        };

        let results_not_matched: Vec<Solution> = {
            let solver = Solver::default();
            // file[path = <path>, size = <size>, contents = "whoa there"]
            let goal = DESCRIPTOR
                .builder()
                .set(&PATH, Scalar::Path(path))
                .set(&CONTENTS, Scalar::String("whoa there".to_string()))
                .goal();
            solver.solve(&goal).collect()
        };

        assert_eq!(results_matched.len(), 1);
        assert_eq!(results_not_matched.len(), 0);
        Ok(())
    }
}
