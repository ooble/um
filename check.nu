#!/usr/bin/env nu

use std

def run [name operation] {
  print --stderr $"+ ($name)"
  do $operation
}

def main [--skip-nix] {
  run 'cargo build' { cargo build --all-targets }
  run 'cargo nextest run' { cargo nextest run --no-fail-fast }
  run 'cargo clippy' { cargo clippy }
  run 'cargo fmt' { cargo fmt --check }
  run 'cargo machete' { cargo machete }

  if not $skip_nix {
    run 'nix build' { nix --no-warn-dirty build }
    run 'nix flake check' { nix --no-warn-dirty flake check }
  }
}
