# Um

"Um" is the magic word that British people use to request something.

For example, imagine you have, for some reason, left your home, and are in a restaurant.

You have a menu. After either a ridiculously short or interminably long time, someone comes to ask you what you would like to eat.

And then you utter the magic words:

> Um, fish and chips.

As if by magic, 20 minutes later, fish and chips arrive.
