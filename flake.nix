{
  description = "um";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
    };
  };

  outputs =
    { self
    , flake-utils
    , nixpkgs
    , fenix
    , crane
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          fenix.overlays.default
        ];
      };
      rustToolchain = fenix.packages.${system}.fromToolchainFile {
        dir = ./.;
        sha256 = "s1RPtyvDGJaX/BisLT+ifVfuhDT1nZkZ1NcK8sbwELM=";
      };
      craneLib = (crane.mkLib pkgs).overrideToolchain rustToolchain;

      buildPackageArgs = {
        pname = "um";

        src = pkgs.lib.cleanSourceWith {
          name = "source";
          src = ./.;
          filter = path: type:
            craneLib.filterCargoSources path type
            || builtins.match ".*\\.lalrpop$" path != null;
        };

        strictDeps = true;

        buildInputs = pkgs.lib.optionals pkgs.stdenv.isDarwin [
          pkgs.libiconv
          pkgs.darwin.apple_sdk.frameworks.CoreFoundation
          pkgs.darwin.apple_sdk.frameworks.Security
          pkgs.darwin.apple_sdk.frameworks.SystemConfiguration
        ];
      };
    in
    {
      formatter = pkgs.nixpkgs-fmt;

      packages.deps = craneLib.buildDepsOnly buildPackageArgs;
      packages.default = craneLib.buildPackage (buildPackageArgs // {
        cargoArtifacts = self.packages.${system}.deps;
      });

      devShells.default = pkgs.mkShell {
        inputsFrom = [self.packages.${system}.default];

        nativeBuildInputs = [
          # build
          rustToolchain
          pkgs.bacon
          pkgs.cargo-edit
          pkgs.cargo-insta
          pkgs.cargo-machete
          pkgs.cargo-nextest

          # testing
          pkgs.nushell
        ];
      };
    });
}
